/*
 * Constants for binding NetModule NBHW12 FGPA GPIO
 *
 * Copyright 2018 NetModule AG.
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 */

#ifndef _DT_BINDINGS_GPIO_NM_NBHW12_FPGA_GPIO_H
#define _DT_BINDINGS_GPIO_NM_NBHW12_FPGA_GPIO_H

/* TODO: delete the following line when we switch to GPIO DT bindings */
#define NM_NBHW12_FPGA_GPIO_BASE 64

#define NM_NBHW12_FPGA_GPIO_STS_WAKE_0 0
#define NM_NBHW12_FPGA_GPIO_STS_WAKE_1 1

#endif /* _DT_BINDINGS_GPIO_NM_NBHW12_FPGA_GPIO_H */
