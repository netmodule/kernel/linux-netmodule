/*
 * Constants for NetModule userspace GPIO bindings
 *
 * Copyright 2018 NetModule AG.
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 */

#ifndef _DT_BINDINGS_GPIO_NM_GPIO_H
#define _DT_BINDINGS_GPIO_NM_GPIO_H

#define NM_GPIO_IN       0
#define NM_GPIO_OUT_LOW  1
#define NM_GPIO_OUT_HIGH 2

#endif /* _DT_BINDINGS_GPIO_NM_GPIO_H */
