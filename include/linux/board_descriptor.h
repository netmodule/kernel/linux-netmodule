/*
 *  linux/include/linux/board_descriptor.h
 *
 * Copyright (C) 2011 NetModule AG
 *
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 */
#ifndef _BOARD_DESCRIPTOR_H
#define _BOARD_DESCRIPTOR_H

/*-------------------------------------------------------------------------*/

#include <linux/nvmem-consumer.h>
#include <linux/proc_fs.h>

/*-------------------------------------------------------------------------*/

/**
 * Get BD element name
 *
 * @param[in]     index
 * @return        string
 */
const char * board_descriptor_get_name (int index);

/*-------------------------------------------------------------------------*/

/**
 * Get PD value from BD
 *
 * @param[in]     slot
 * @param[in]     pd
 * @param[in]     buf
 * @param[in]     bufsize
 * @return        0 on success
 */
int board_decriptor_get_pd (int slot, char *pd, char *buf, size_t bufsize);

/**
 * Registers an additional board descriptor.
 * Reads all registered board descriptors from non-volatile memory
 * and creates a proc file system for them.
 *
 * @param[in]     mem_acc					Memory accessor to access the non-volatile memory
 * @param[in]     board_descriptor_offset   Offset of the board descriptor
 * 											within non-volatile memory
 * @return        0 on success. Otherwise a negative value
 */
extern int board_descriptor_register(struct nvmem_device *mem_acc, int board_descriptor_offset,
                                     const unsigned char* hmac_key, int hmac_key_length, int slot);

/*-------------------------------------------------------------------------*/

/**
 * Returns the serial number from a board descriptor
 *
 * @param[in]     slot			Index identifying the board descriptor
 * 								to read the data from.
 * @param[in]     serial		Buffer to store the serial number
 * @param[in]     bufsize		Length of the provided buffer
 * @return        0 on success. Otherwise a negative value
 */
extern int board_decriptor_get_serial_number(int slot, char* serial, size_t bufsize);

/*-------------------------------------------------------------------------*/

/**
 * Returns an ethernet MAC address from a board descriptor
 *
 * @param[in]     slot			Index identifying the board descriptor
 * 								to read the data from.
 * @param[in]     index			Index of the MAC address to retrieve
 * @param[in]     pMac			Buffer to store the 6 byte MAC address
 * @return        0 on success. Otherwise a negative value
 */
int board_decriptor_get_ethernet_mac(int slot, unsigned int index, unsigned char* pMac);

/*-------------------------------------------------------------------------*/

/**
 * Returns the hardware version numbers from a board descriptor
 *
 * @param[in]     slot			Index identifying the board descriptor
 * 								to read the data from.
 * @param[in]     pVer			Pointer to a variable to receive
 * 								the hardware version.
 * @param[in]     pRev			Pointer to a variable to receive
 * 								the hardware revision.
 * @return        0 on success. Otherwise a negative value
 */
extern int board_decriptor_get_hw_version(int slot, int* pVer, int* pRev);

/*-------------------------------------------------------------------------*/

/**
 * Returns the product name from a board descriptor
 *
 * @param[in]     slot			Index identifying the board descriptor
 * 								to read the data from.
 * @param[in]     serial		Buffer to store the product name
 * @param[in]     bufsize		Length of the provided buffer
 * @return        0 on success. Otherwise a negative value
 */
extern int board_decriptor_get_product_name(int slot, char* name, size_t bufsize);

/*-------------------------------------------------------------------------*/

/**
 * Returns the requested product descriptor
 *
 * @param[in]     slot			Index identifying the board descriptor to read the data from.
 *                                      (-1 for all boards)
 * @param[in]     pd                    product descriptor tag
 * @param[in]     buf                   buffer to store the tags value.
 * @param[in]     bufsize               size of buffer
 *
 * @return        0 on success. Otherwise a negative value
 */
extern int board_decriptor_get_pd(int slot, char *pd, char *buf, size_t bufsize);


/**
 * Returns the requested product descriptor tag
 */
extern int netbox_get_pdtag (char *name, char *tag, char *dst, size_t dstlen);

#endif
