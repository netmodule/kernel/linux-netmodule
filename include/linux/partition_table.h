/*
 *  linux/include/linux/partition_table.h
 *
 * Copyright (C) 2011 NetModule AG
 *
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 */
#ifndef _PARTITION_TABLE_H
#define _PARTITION_TABLE_H

/**
 * Reads the partition table from non-volatile memory and creates a proc
 * file system to access the information stored in it.
 *
 * @param[in]     mem_acc					Memory accessor to access the non-volatile memory
 * @param[in]     partition_table_offset    Offset of the partition table
 * 											within non-volatile memory
 * @return        0 on success. Otherwise a negative value
 */
extern int partition_table_init(struct nvmem_device *mem_acc, int partition_table_offset);


#endif
