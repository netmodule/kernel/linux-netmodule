#ifndef _NETBOX_SYSTEM_INFO_H
#define _NETBOX_SYSTEM_INFO_H

extern struct proc_dir_entry * netbox_system_info_dir;
extern int netbox_board_descriptor_valid;

extern int netbox_system_info_prepare(void);
extern int netbox_system_info_init(void);

extern int netbox_get_pdtag (char *name, char *tag, char *dst, size_t dstlen);
extern int wwan_interface_index_from_slot (int slot);

extern int pcie_init_gnss_power (int slot);
extern int pcie_read_gnss_power(int slot);
extern int pcie_write_gnss_power (int slot, int on);

extern int pcie_carrier_present (void);

extern int pcie_carrier_module_bus (void);
extern int pcie_carrier_sim_bus (int index);
extern int pcie_carrier_sim_present (int index);
extern int pcie_carrier_sim_changed (int index);
extern int pcie_carrier_sim_disconnect (int index);
extern int pcie_carrier_sim_connect (int index, int bus);


#endif /* _NETBOX_SYSTEM_INFO_H */
