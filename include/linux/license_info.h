#ifndef LICENSE_INFO_H
#define LICENSE_INFO_H
/******************************************************************************
 * (c) COPYRIGHT 2009 by NetModule AG, Switzerland.  All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * PACKAGE : Netmodule
 *
 * ABSTRACT:
 *  This file evaluates the license info stored in the NetBox EEPROM.
 *
 * HISTORY:
 *  Date      Author       Description
 *  20091013  alu          created
 *  20091127  alu          RFE-FB18113: modifications on /proc files
 *
 *****************************************************************************/

/*--- includes ---------------------------------------------------------------*/

#include <linux/nvmem-consumer.h>
#include <linux/proc_fs.h>

/*--- defines ----------------------------------------------------------------*/
#define LICENSE_INFO_SIZE                     0x20

#define NUMBER_OF_LICENSE_FEATURES            64
#define NUMBER_OF_LICENSE_OPTIONS             11

/* assign a license feature number (0-63) to each of the licensed features */
#define LICENSE_FEATURE_NUMBER_UMTS            0
#define LICENSE_FEATURE_NUMBER_GSM             1
#define LICENSE_FEATURE_NUMBER_GPS             2
#define LICENSE_FEATURE_NUMBER_WLAN            3
#define LICENSE_FEATURE_NUMBER_MOBILE_IP       4 /* obsolete */
#define LICENSE_FEATURE_NUMBER_LTE             5
#define LICENSE_FEATURE_NUMBER_VOICE           6
#define LICENSE_FEATURE_NUMBER_SERVER          7
#define LICENSE_FEATURE_NUMBER_VIRT            8

/* assign a license option number (0-11) to each of the license options (no options used yet) */
/* #define LICENSE_OPTION_NUMBER_XXXX            0 */


#define EEPROM_CHECKSUM_SIZE                   2
#define MAC_ADDRESS_SIZE                       6

/* check parameters */
#if NUMBER_OF_LICENSE_FEATURES%8 != 0
  #error "Invalid number of license features!\n"
#endif
#if (NUMBER_OF_LICENSE_FEATURES/8 + NUMBER_OF_LICENSE_OPTIONS*2) > (LICENSE_INFO_SIZE - EEPROM_CHECKSUM_SIZE)
  #error "Too many license features or options!\n"
#endif

#define LICENSE_FILE_MAX_SIZE               2048


/*--- types ------------------------------------------------------------------*/

/*--- globals ----------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/

/**
 * Checks if the specified license feature is enabled.
 *
 * @param            featureNumber (I): The license feature to be checked.
 * @return           1, if the license feature is enabled.
 *                   0, if the license feature is disabled or does not exist
 */
unsigned int licenseInfoGetFeature(unsigned int featureNumber);

/**
 * Returns the current value of the specified option.
 *
 * @param            optionNumber (I): The option number to be returned.
 * @return           The current value of the specified option.
 */
u16 licenseInfoGetOption(unsigned int optionNumber);

/**
 * Updates the license with the new license file.
 *
 * @param            licenseFileBuffer (I): The new license file to update
 *                                          the license information.
 * @return           0, if the license update has been successful.
 *                   negative error code if the license update failed.
 */
int licenseInfoUpdateLicense(const char *licenseFileBuffer);

/**
 * Initializes the licensing system
 *
 * @param            mem_acc: Interface to access the eeprom
 * @param            license_data_offset: Offset within EEPROM at which the licensing
 *										information is stored
 * @param            is_board_descriptor_valid: Flag indicating, if the current board
 *										descriptor including its hash is valid
 * @param            clear_license_info_buffer_on_invalid_checksum: Flag indicating,
 *										if the license buffer shall be cleared, if its
 *										checksum is invalid.
 */
void license_info_init(struct nvmem_device *mem_acc,
                       int license_data_offset,
                       int clear_license_info_buffer_on_invalid_checksum);


#endif /* LICENSE_INFO_H */
