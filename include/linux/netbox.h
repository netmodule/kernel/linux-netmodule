#ifndef _NETBOX_H
#define _NETBOX_H

#include <linux/board_descriptor.h>
#include <linux/license_info.h>
#include <linux/partition_table.h>
#include <linux/bdparser.h>
#include <linux/netbox_system_info.h>
//#include <nbsw.h>

extern int NBHW;
extern int HDEADBOLT;

extern int NBCPver;
extern int NBCPrel;
extern int NBMCver;
extern int NBMCrel;
extern int NBPSEver;
extern int NBPSErel;

#endif /* _NETBOX_H */
