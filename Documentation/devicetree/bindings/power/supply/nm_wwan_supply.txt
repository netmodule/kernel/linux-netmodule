NetModule WWAN supply controller

Required properties:
- compatible: must be "nm,wwan-supply"
- wwan-supply: regulator which is controlling the supply voltage of the WWAN modem.

Example:

	wwan0-supply {
		compatible = "nm,wwan-supply";
		wwan-supply = <&wwan_fixed>;
	};
