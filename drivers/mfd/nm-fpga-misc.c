/*
 * nm-fpga-misc.c - NetModule FPGA miscellaneous functionality driver
 *
 * Copyright (C) 2018 NetModule AG
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/of_device.h>

#include <linux/regmap.h>
#include <linux/sysfs.h>

#include "nm-fpga.h"
#include "../misc/nm-sysfs-dev.h"

#define NM_FPGA_REG_SCRATCH       0x0004

static inline struct nm_fpga* nm_misc_get_fpga(struct device *dev)
{
	return dev_get_drvdata(dev->parent);
}

static ssize_t scratch_show(struct device *dev, struct device_attribute *attr,
			    char *buf)
{
	unsigned int val;
	regmap_read(nm_misc_get_fpga(dev)->regmap, NM_FPGA_REG_SCRATCH, &val);
	return snprintf(buf, PAGE_SIZE, "0x%04X\n", val);
}

static ssize_t scratch_store(struct device *dev, struct device_attribute *attr,
			     const char *buf, size_t count)
{
	unsigned long val;
	int ret;

	ret = kstrtoul(buf, 0, &val);
	if (ret) {
		printk(KERN_ERR "%s: cannot convert string \"%s\" to int\n",
		       __FUNCTION__, buf);
		return ret;
	}
	if (val > U16_MAX) {
		printk(KERN_ERR "%s: value out of range: %s\n", __FUNCTION__,
		       buf);
		return -ERANGE;
	}

	regmap_write(nm_misc_get_fpga(dev)->regmap, NM_FPGA_REG_SCRATCH, val);
	return count;
}

static DEVICE_ATTR_RW(scratch);

static int nm_fpga_misc_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	int ret;
	if (!nm_sysfs_dev)
		return -EPROBE_DEFER;

	/* File in sysfs: /sys/devices/netmodule/fpga/scratch */
	ret = device_create_file(dev, &dev_attr_scratch);
	if (ret) {
		dev_err(dev, "device_create_file() failed: %d\n", ret);
		return ret;
	}

	ret = sysfs_create_link(&nm_sysfs_dev->kobj, &dev->kobj, "fpga");
	if (ret) {
		dev_err(dev, "sysfs_create_link() failed: %d\n", ret);
		goto error;
	}
	dev_info(dev, "%s completed successfully\n", __FUNCTION__);
	return 0;
error:
	device_remove_file(dev, &dev_attr_scratch);
	return ret;
}

static int nm_fpga_misc_remove(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	device_remove_file(dev, &dev_attr_scratch);
	dev_info(dev, "%s completed successfully\n", __FUNCTION__);
	return 0;
}

static const struct of_device_id nm_fpga_misc_of_match[] = {
	{ .compatible = "netmodule,fpga-misc" },
	{ },
};
MODULE_DEVICE_TABLE(of, nm_fpga_misc_of_match);

static struct platform_driver nm_fpga_misc_driver = {
	.probe = nm_fpga_misc_probe,
	.remove = nm_fpga_misc_remove,
	.driver = {
		.name = "nm-fpga-misc",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(nm_fpga_misc_of_match),
	},
};
module_platform_driver(nm_fpga_misc_driver);

MODULE_DESCRIPTION("NetModule FPGA miscellaneous functionality driver");
MODULE_AUTHOR("NetModule AG");
MODULE_LICENSE("GPL");
