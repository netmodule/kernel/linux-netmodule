/*
 * nm-fpga-gpio17.c - NetModule FPGA GPIO driver for NBHW17
 *
 * Copyright (C) 2018 NetModule AG
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/export.h>
#include <linux/bug.h>
#include <linux/kernel.h>

#include "nm-fpga-gpio-conf.h"
#include <dt-bindings/gpio/nm-nbhw17-fpga-gpio.h>

#define NM_NBHW17_FPGA_REG_OUTPUT1           0x0008

#define NM_NBHW17_FPGA_REG_INT_MASK1         0x0010
#define NM_NBHW17_FPGA_REG_INT_MASK2         0x0012
#define NM_NBHW17_FPGA_REG_INT_PENDING1      0x0014
#define NM_NBHW17_FPGA_REG_INT_PENDING2      0x0016
#define NM_NBHW17_FPGA_REG_INT_STATUS1       0x0018
#define NM_NBHW17_FPGA_REG_INT_STATUS2       0x001A
#define NM_NBHW17_FPGA_REG_INT_ACK1          0x0018
#define NM_NBHW17_FPGA_REG_INT_ACK2          0x001A

#define NM_NBHW17_FPGA_REG_LED               0x0020
#define NM_NBHW17_FPGA_REG_PCIE_RESET        0x0030
#define NM_NBHW17_FPGA_REG_PCIE_POWER        0x0032

#define NM_NBHW17_FPGA_REG_PCIE_1_SLOT_DIR1  0x0100
#define NM_NBHW17_FPGA_REG_PCIE_1_SLOT_CTRL1 0x0102

#define NM_NBHW17_FPGA_REG_PCIE_2_SLOT_DIR1  0x0200
#define NM_NBHW17_FPGA_REG_PCIE_2_SLOT_CTRL1 0x0202

#define NM_NBHW17_FPGA_REG_PCIE_3_SLOT_DIR1  0x0300
#define NM_NBHW17_FPGA_REG_PCIE_3_SLOT_CTRL1 0x0302

#define NM_NBHW17_FPGA_REG_PCIE_4_SLOT_DIR1  0x0400
#define NM_NBHW17_FPGA_REG_PCIE_4_SLOT_CTRL1 0x0402

#define NM_NBHW17_FPGA_REG_PCIE_SLOT_PCM_PIN_GPIO_DATA_REGISTER_4 0x0434

static struct nm_fpga_gpio_reg_init reg_17[] = {
	{NM_NBHW17_FPGA_REG_LED, 0x0001},

	/* Write to control registers first (to avoid unnecessary switching of
	 * output signal, search for "FPGA IO direction change semantics" in
	 * nm-fpga-gpio.c). */
	{NM_NBHW17_FPGA_REG_PCIE_1_SLOT_CTRL1, 0x0020},
	{NM_NBHW17_FPGA_REG_PCIE_1_SLOT_DIR1,  0x0020},

	{NM_NBHW17_FPGA_REG_PCIE_2_SLOT_CTRL1, 0x0020},
	{NM_NBHW17_FPGA_REG_PCIE_2_SLOT_DIR1,  0x0020},

	{NM_NBHW17_FPGA_REG_PCIE_3_SLOT_CTRL1, 0x0020},
	{NM_NBHW17_FPGA_REG_PCIE_3_SLOT_DIR1,  0x0020},

	{NM_NBHW17_FPGA_REG_PCIE_4_SLOT_CTRL1, 0x0020},
	{NM_NBHW17_FPGA_REG_PCIE_4_SLOT_DIR1,  0x0020},
};

static struct nm_fpga_gpio_line line_17[] = {
	[NM_NBHW17_FPGA_GPIO_LED0_GREEN] = {
		.reg = NM_NBHW17_FPGA_REG_LED,
		.bit = 0,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_LED0_RED] = {
		.reg = NM_NBHW17_FPGA_REG_LED,
		.bit = 1,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_LED1_GREEN] = {
		.reg = NM_NBHW17_FPGA_REG_LED,
		.bit = 2,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_LED1_RED] = {
		.reg = NM_NBHW17_FPGA_REG_LED,
		.bit = 3,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_LED2_GREEN] = {
		.reg = NM_NBHW17_FPGA_REG_LED,
		.bit = 4,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_LED2_RED] = {
		.reg = NM_NBHW17_FPGA_REG_LED,
		.bit = 5,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_LED3_GREEN] = {
		.reg = NM_NBHW17_FPGA_REG_LED,
		.bit = 6,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_LED3_RED] = {
		.reg = NM_NBHW17_FPGA_REG_LED,
		.bit = 7,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_LED4_GREEN] = {
		.reg = NM_NBHW17_FPGA_REG_LED,
		.bit = 8,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_LED4_RED] = {
		.reg = NM_NBHW17_FPGA_REG_LED,
		.bit = 9,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_LED5_GREEN] = {
		.reg = NM_NBHW17_FPGA_REG_LED,
		.bit = 10,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_LED5_RED] = {
		.reg = NM_NBHW17_FPGA_REG_LED,
		.bit = 11,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_LED6_GREEN] = {
		.reg = NM_NBHW17_FPGA_REG_LED,
		.bit = 12,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_LED6_RED] = {
		.reg = NM_NBHW17_FPGA_REG_LED,
		.bit = 13,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_LED7_GREEN] = {
		.reg = NM_NBHW17_FPGA_REG_LED,
		.bit = 14,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_LED7_RED] = {
		.reg = NM_NBHW17_FPGA_REG_LED,
		.bit = 15,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},

	[NM_NBHW17_FPGA_GPIO_PCIE1_RST] = {
		.reg = NM_NBHW17_FPGA_REG_PCIE_RESET,
		.bit = 0,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_PCIE2_RST] = {
		.reg = NM_NBHW17_FPGA_REG_PCIE_RESET,
		.bit = 1,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_PCIE3_RST] = {
		.reg = NM_NBHW17_FPGA_REG_PCIE_RESET,
		.bit = 2,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_PCIE4_RST] = {
		.reg = NM_NBHW17_FPGA_REG_PCIE_RESET,
		.bit = 3,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},

	[NM_NBHW17_FPGA_GPIO_PCIE1_PWR] = {
		.reg = NM_NBHW17_FPGA_REG_PCIE_POWER,
		.bit = 0,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_PCIE2_PWR] = {
		.reg = NM_NBHW17_FPGA_REG_PCIE_POWER,
		.bit = 1,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_PCIE3_PWR] = {
		.reg = NM_NBHW17_FPGA_REG_PCIE_POWER,
		.bit = 2,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_PCIE4_PWR] = {
		.reg = NM_NBHW17_FPGA_REG_PCIE_POWER,
		.bit = 3,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},

	[NM_NBHW17_FPGA_GPIO_PCIE1_WDIS] = {
		.reg = NM_NBHW17_FPGA_REG_PCIE_1_SLOT_CTRL1,
		.bit = 5,
		.flags = NM_GPIO_FLAG_READ_WRITE,
		.has_IO_direction_control = 1,
	},
	[NM_NBHW17_FPGA_GPIO_PCIE2_WDIS] = {
		.reg = NM_NBHW17_FPGA_REG_PCIE_2_SLOT_CTRL1,
		.bit = 5,
		.flags = NM_GPIO_FLAG_READ_WRITE,
		.has_IO_direction_control = 1,
	},
	[NM_NBHW17_FPGA_GPIO_PCIE3_WDIS] = {
		.reg = NM_NBHW17_FPGA_REG_PCIE_3_SLOT_CTRL1,
		.bit = 5,
		.flags = NM_GPIO_FLAG_READ_WRITE,
		.has_IO_direction_control = 1,
	},
	[NM_NBHW17_FPGA_GPIO_PCIE4_WDIS] = {
		.reg = NM_NBHW17_FPGA_REG_PCIE_4_SLOT_CTRL1,
		.bit = 5,
		.flags = NM_GPIO_FLAG_READ_WRITE,
		.has_IO_direction_control = 1,
	},

	[NM_NBHW17_FPGA_GPIO_HOLD_PWR_ON] = {
		.reg = NM_NBHW17_FPGA_REG_OUTPUT1,
		.bit = 0,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_EN_GPS_ANT] = {
		.reg = NM_NBHW17_FPGA_REG_OUTPUT1,
		.bit = 1,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_EN_MDIO_PHY] = {
		.reg = NM_NBHW17_FPGA_REG_OUTPUT1,
		.bit = 2,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_EN_MDIO_EXT] = {
		.reg = NM_NBHW17_FPGA_REG_OUTPUT1,
		.bit = 3,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_EN_SATA_PWR] = {
		.reg = NM_NBHW17_FPGA_REG_OUTPUT1,
		.bit = 4,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_SERDES_SEL] = {
		.reg = NM_NBHW17_FPGA_REG_OUTPUT1,
		.bit = 5,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_SERDES_EN] = {
		.reg = NM_NBHW17_FPGA_REG_OUTPUT1,
		.bit = 6,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_RST_EXT] = {
		.reg = NM_NBHW17_FPGA_REG_OUTPUT1,
		.bit = 7,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW17_FPGA_GPIO_RST_EXT_ETH] = {
		.reg = NM_NBHW17_FPGA_REG_OUTPUT1,
		.bit = 8,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},

	[NM_NBHW17_FPGA_GPIO_PCIE1_WAKE] = {
		.reg = NM_NBHW17_FPGA_REG_PCIE_1_SLOT_CTRL1,
		.bit = 4,
		.flags = NM_GPIO_FLAG_READ_WRITE,
		.has_IO_direction_control = 1,
	},
	[NM_NBHW17_FPGA_GPIO_PCIE2_WAKE] = {
		.reg = NM_NBHW17_FPGA_REG_PCIE_2_SLOT_CTRL1,
		.bit = 4,
		.flags = NM_GPIO_FLAG_READ_WRITE,
		.has_IO_direction_control = 1,
	},
	[NM_NBHW17_FPGA_GPIO_PCIE3_WAKE] = {
		.reg = NM_NBHW17_FPGA_REG_PCIE_3_SLOT_CTRL1,
		.bit = 4,
		.flags = NM_GPIO_FLAG_READ_WRITE,
		.has_IO_direction_control = 1,
	},
	[NM_NBHW17_FPGA_GPIO_PCIE4_WAKE] = {
		.reg = NM_NBHW17_FPGA_REG_PCIE_4_SLOT_CTRL1,
		.bit = 4,
		.flags = NM_GPIO_FLAG_READ_WRITE,
		.has_IO_direction_control = 1,
	},

	[NM_NBHW17_FPGA_GPIO_REG_4_PCIE_PIN_47_RW] = {
		.reg = NM_NBHW17_FPGA_REG_PCIE_SLOT_PCM_PIN_GPIO_DATA_REGISTER_4,
		.bit = 1,
		.flags = NM_GPIO_FLAG_READ_WRITE,
		.has_IO_direction_control = 1,
		.irq_reg = 1,
		.irq_reg_bit = 7,
	},
	[NM_NBHW17_FPGA_GPIO_REG_4_PCIE_PIN_49_RW] = {
		.reg = NM_NBHW17_FPGA_REG_PCIE_SLOT_PCM_PIN_GPIO_DATA_REGISTER_4,
		.bit = 2,
		.flags = NM_GPIO_FLAG_READ_WRITE,
		.has_IO_direction_control = 1,
		.irq_reg = 1,
		.irq_reg_bit = 8,
	},
};

struct nm_fpga_gpio_irq_desc irq_17[] = {
	{
		.IntMask_reg    = NM_NBHW17_FPGA_REG_INT_MASK1,
		.IntPending_reg = NM_NBHW17_FPGA_REG_INT_PENDING1,
		.IntStatus_reg  = NM_NBHW17_FPGA_REG_INT_STATUS1,
		.IntAck_reg     = NM_NBHW17_FPGA_REG_INT_ACK1,
		.mask = 0x0180,
	},
	{
		.IntMask_reg    = NM_NBHW17_FPGA_REG_INT_MASK2,
		.IntPending_reg = NM_NBHW17_FPGA_REG_INT_PENDING2,
		.IntStatus_reg  = NM_NBHW17_FPGA_REG_INT_STATUS2,
		.IntAck_reg     = NM_NBHW17_FPGA_REG_INT_ACK2,
		.mask = 0x0000,
	},
};

static unsigned get_offset_17(unsigned irq_reg_idx, unsigned bit)
{
	switch (irq_reg_idx) {
	case 0:
		switch (bit) {
		case 7:
			return NM_NBHW17_FPGA_GPIO_REG_4_PCIE_PIN_47_RW;
		case 8:
			return NM_NBHW17_FPGA_GPIO_REG_4_PCIE_PIN_49_RW;
		default:
			BUG();
		}
		break;
	default:
		BUG();
	}
	return 0;
}

DEFINE_GPIO_CONF(17);
