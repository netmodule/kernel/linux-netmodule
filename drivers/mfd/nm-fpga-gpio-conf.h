/*
 * nm-fpga-gpio-conf.h - NetModule FPGA GPIO driver
 *
 * Copyright (C) 2018 NetModule AG
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef __MFD_NM_FPGA_GPIO_CONF_H
#define __MFD_NM_FPGA_GPIO_CONF_H

#include "nm-fpga-gpio.h"

#define NM_GPIO_FLAG_INVALID_GPIO 0
#define NM_GPIO_FLAG_READ (1 << 0)
#define NM_GPIO_FLAG_WRITE (1 << 1)
#define NM_GPIO_FLAG_READ_WRITE (NM_GPIO_FLAG_READ | NM_GPIO_FLAG_WRITE)

struct nm_fpga_gpio_line {
	u16 reg;
	u16 bit:4; /* in reg */
	u16 flags:2; /* see NM_GPIO_FLAG_* definitions above */
	u16 has_IO_direction_control:1;
	u16 irq_reg:2; /* 1 for IntMask1, IntPending1, ... 2 for IntMask2, ...*/
	u16 irq_reg_bit:4; /* in IRQ register */
};

struct nm_fpga_gpio_reg_init {
	u16 address;
	u16 value;
};

struct nm_fpga_gpio_irq_desc {
	u16 IntMask_reg;
	u16 IntPending_reg;
	u16 IntStatus_reg;
	u16 IntAck_reg;
	u16 mask; /* see get_irq_mask() in nm-fpga-gpio.h */
};

#define DEFINE_INIT_FPGA(nbhw) \
	static void init_fpga_##nbhw(struct regmap* regmap)                    \
	{                                                                      \
		unsigned i;                                                    \
		for (i = 0; i < ARRAY_SIZE(reg_##nbhw); ++i)                   \
			regmap_write(regmap, reg_##nbhw[i].address,            \
				reg_##nbhw[i].value);                          \
	}

#define DEFINE_GET_NUM_GPIOS(nbhw)                                             \
	static unsigned get_num_gpios_##nbhw(void)                             \
	{                                                                      \
		return ARRAY_SIZE(line_##nbhw);                                \
	}

#define DEFINE_GET_GPIO_CHIP_BASE(nbhw) \
	static int get_gpio_chip_base_##nbhw(void)                             \
	{                                                                      \
		return NM_NBHW##nbhw##_FPGA_GPIO_BASE;                         \
	}

#define DEFINE_GET_REG(nbhw) \
	static u16 get_reg_##nbhw(unsigned offset)                             \
	{                                                                      \
		return line_##nbhw[offset].reg;                                \
	}

#define DEFINE_GET_BIT(nbhw) \
	static unsigned get_bit_##nbhw(unsigned offset)                        \
	{                                                                      \
		return line_##nbhw[offset].bit;                                \
	}

#define DEFINE_CAN_READ(nbhw) \
	static bool can_read_##nbhw(unsigned offset)                           \
	{                                                                      \
		return (line_##nbhw[offset].flags & NM_GPIO_FLAG_READ) != 0;   \
	}

#define DEFINE_CAN_WRITE(nbhw) \
	static bool can_write_##nbhw(unsigned offset)                          \
	{                                                                      \
		return (line_##nbhw[offset].flags & NM_GPIO_FLAG_WRITE) != 0;  \
	}

#define DEFINE_HAS_IRQ(nbhw) \
	static bool has_irq_##nbhw(unsigned offset)                            \
	{                                                                      \
		return line_##nbhw[offset].irq_reg != 0;                       \
	}

#define DEFINE_HAS_IO_DIRECTION_CONTROL(nbhw) \
	static bool has_IO_direction_control_##nbhw(unsigned offset)           \
	{                                                                      \
		return line_##nbhw[offset].has_IO_direction_control != 0;      \
	}

#define DEFINE_GET_IO_DIRECTION_REG(nbhw) \
	static u16 get_IO_direction_reg_##nbhw(unsigned offset)                \
	{                                                                      \
		return get_reg_##nbhw(offset) - 2;                             \
	}

#define DEFINE_GET_IO_DIRECTION_BIT(nbhw) \
	static unsigned get_IO_direction_bit_##nbhw(unsigned offset)           \
	{                                                                      \
		return get_bit_##nbhw(offset);                                 \
	}

#define DEFINE_IS_VALID(nbhw) \
	static bool is_valid_##nbhw(unsigned offset)                           \
	{                                                                      \
		if (offset >= ARRAY_SIZE(line_##nbhw))                         \
			return false;                                          \
		return line_##nbhw[offset].flags != 0;                         \
	}

#ifdef NM_FPGA_GPIO_NO_INTERRUPTS

#define DEFINE_IRQ_MEMBERS(nbhw) \
		.get_num_irq_regs = get_num_irq_regs_##nbhw,                   \

#define DEFINE_GET_NUM_IRQ_REGS(nbhw) \
	static unsigned get_num_irq_regs_##nbhw(void)                          \
	{                                                                      \
		return 0;                                                      \
	}

/* Rest functions will not be defined, corresponding members of
 * struct nm_fpga_gpio_config will be zero-initialized (we should never call
 * them). */
#define DEFINE_GET_IRQ_MASK(nbhw)
#define DEFINE_GET_OFFSET(nbhw)
#define DEFINE_GET_IRQ_REG_IDX(nbhw)
#define DEFINE_GET_IRQ_REG_BIT(nbhw)
#define DEFINE_GET_INT_REG(nbhw, IntMask)

#else

#define DEFINE_IRQ_MEMBERS(nbhw) \
		.get_num_irq_regs = get_num_irq_regs_##nbhw,                   \
		.get_irq_mask = get_irq_mask_##nbhw,                           \
		.get_offset = get_offset_##nbhw,                               \
		.get_irq_reg_idx = get_irq_reg_idx_##nbhw,                     \
		.get_irq_reg_bit = get_irq_reg_bit_##nbhw,                     \
		.get_IntMask_reg = get_IntMask_reg_##nbhw,                     \
		.get_IntPending_reg = get_IntPending_reg_##nbhw,               \
		.get_IntStatus_reg = get_IntStatus_reg_##nbhw,                 \
		.get_IntAck_reg = get_IntAck_reg_##nbhw,                       \

#define DEFINE_GET_NUM_IRQ_REGS(nbhw) \
	static unsigned get_num_irq_regs_##nbhw(void)                          \
	{                                                                      \
		return ARRAY_SIZE(irq_##nbhw);                                 \
	}

#define DEFINE_GET_IRQ_MASK(nbhw) \
	static u16 get_irq_mask_##nbhw(unsigned irq_reg_idx)                   \
	{                                                                      \
		BUG_ON(irq_reg_idx >= ARRAY_SIZE(irq_##nbhw));                 \
		return irq_##nbhw[irq_reg_idx].mask;                           \
	}

#define DEFINE_GET_OFFSET(nbhw) /* to be defined in NBHW-specific code */

#define DEFINE_GET_IRQ_REG_IDX(nbhw) \
	static unsigned get_irq_reg_idx_##nbhw(unsigned offset)                \
	{                                                                      \
		BUG_ON(!line_##nbhw[offset].irq_reg);                          \
		return line_##nbhw[offset].irq_reg - 1;                        \
	}

#define DEFINE_GET_IRQ_REG_BIT(nbhw) \
	static unsigned get_irq_reg_bit_##nbhw(unsigned offset)                \
	{                                                                      \
		BUG_ON(!line_##nbhw[offset].irq_reg);                          \
		return line_##nbhw[offset].irq_reg_bit;                        \
	}

#define DEFINE_GET_INT_REG(nbhw, reg_name) \
	static u16 get_##reg_name##_reg_##nbhw(unsigned irq_reg_idx)           \
	{                                                                      \
		BUG_ON(irq_reg_idx >= ARRAY_SIZE(irq_##nbhw));                 \
		return irq_##nbhw[irq_reg_idx].reg_name##_reg;                 \
	}

#endif /* NM_FPGA_GPIO_NO_INTERRUPTS */

#define DEFINE_EXPORTED_STRUCT(nbhw) \
	struct nm_fpga_gpio_config nm_fpga_gpio_##nbhw = {                     \
		.init_fpga = init_fpga_##nbhw,                                 \
		.get_num_gpios = get_num_gpios_##nbhw,                         \
		.get_gpio_chip_base = get_gpio_chip_base_##nbhw,               \
		.get_reg = get_reg_##nbhw,                                     \
		.get_bit = get_bit_##nbhw,                                     \
		.can_read = can_read_##nbhw,                                   \
		.can_write = can_write_##nbhw,                                 \
		.has_irq = has_irq_##nbhw,                                     \
		.has_IO_direction_control = has_IO_direction_control_##nbhw,   \
		.get_IO_direction_reg = get_IO_direction_reg_##nbhw,           \
		.get_IO_direction_bit = get_IO_direction_bit_##nbhw,           \
		.is_valid = is_valid_##nbhw,                                   \
		DEFINE_IRQ_MEMBERS(nbhw)                                       \
	};                                                                     \
	EXPORT_SYMBOL(nm_fpga_gpio_##nbhw)

#define DEFINE_GPIO_CONF(nbhw) \
	DEFINE_INIT_FPGA(nbhw)                                                 \
	DEFINE_GET_NUM_GPIOS(nbhw)                                             \
	DEFINE_GET_GPIO_CHIP_BASE(nbhw)                                        \
	DEFINE_GET_REG(nbhw)                                                   \
	DEFINE_GET_BIT(nbhw)                                                   \
	DEFINE_CAN_READ(nbhw)                                                  \
	DEFINE_CAN_WRITE(nbhw)                                                 \
	DEFINE_HAS_IRQ(nbhw)                                                   \
	DEFINE_HAS_IO_DIRECTION_CONTROL(nbhw)                                  \
	DEFINE_GET_IO_DIRECTION_REG(nbhw)                                      \
	DEFINE_GET_IO_DIRECTION_BIT(nbhw)                                      \
	DEFINE_IS_VALID(nbhw)                                                  \
	DEFINE_GET_NUM_IRQ_REGS(nbhw)                                          \
	DEFINE_GET_IRQ_MASK(nbhw)                                              \
	DEFINE_GET_OFFSET(nbhw)                                                \
	DEFINE_GET_IRQ_REG_IDX(nbhw)                                           \
	DEFINE_GET_IRQ_REG_BIT(nbhw)                                           \
	DEFINE_GET_INT_REG(nbhw, IntMask)                                      \
	DEFINE_GET_INT_REG(nbhw, IntPending)                                   \
	DEFINE_GET_INT_REG(nbhw, IntStatus)                                    \
	DEFINE_GET_INT_REG(nbhw, IntAck)                                       \
	DEFINE_EXPORTED_STRUCT(nbhw)

#endif /* __MFD_NM_FPGA_GPIO_CONF_H */
