/*
 * nm-fpga.h - NetModule FPGA driver
 *
 * Copyright (C) 2018 NetModule AG
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef __MFD_NM_FPGA_H
#define __MFD_NM_FPGA_H

struct nm_fpga {
	struct regmap *regmap;
	int irq;
};

#endif /* __MFD_NM_FPGA_H */
