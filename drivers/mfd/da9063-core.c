// SPDX-License-Identifier: GPL-2.0+
/*
 * Device access for Dialog DA9063 modules
 *
 * Copyright 2012 Dialog Semiconductors Ltd.
 * Copyright 2013 Philipp Zabel, Pengutronix
 *
 * Author: Krystian Garbaciak, Dialog Semiconductor
 * Author: Michal Hajduk, Dialog Semiconductor
 *
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/mutex.h>
#include <linux/mfd/core.h>
#include <linux/regmap.h>
#include <linux/gpio/consumer.h>
#include <linux/regulator/consumer.h>
#include <linux/of.h>

#include <linux/mfd/da9063/core.h>
#include <linux/mfd/da9063/registers.h>

#include <linux/proc_fs.h>
#include <linux/kthread.h>
#include <linux/uaccess.h>

#define MAX_GPIOS 16
#define MAX_LDOS 11
#define MAX_SIGNALS 29
#define N_MASK_REGISTERS 4
#define DA9063_REG_IRQ_MASK_A_RESERVED_BITS_OFFSET 5
#define DA9063_REG_IRQ_MASK_A_N_RESERVED_BITS 3

static struct da9063 *da9063_data;

static struct resource da9063_regulators_resources[] = {
	{
		.name	= "LDO_LIM",
		.start	= DA9063_IRQ_LDO_LIM,
		.end	= DA9063_IRQ_LDO_LIM,
		.flags	= IORESOURCE_IRQ,
	},
};

static struct resource da9063_rtc_resources[] = {
	{
		.name	= "ALARM",
		.start	= DA9063_IRQ_ALARM,
		.end	= DA9063_IRQ_ALARM,
		.flags	= IORESOURCE_IRQ,
	},
	{
		.name	= "TICK",
		.start	= DA9063_IRQ_TICK,
		.end	= DA9063_IRQ_TICK,
		.flags	= IORESOURCE_IRQ,
	}
};

static struct resource da9063_onkey_resources[] = {
	{
		.name	= "ONKEY",
		.start	= DA9063_IRQ_ONKEY,
		.end	= DA9063_IRQ_ONKEY,
		.flags	= IORESOURCE_IRQ,
	},
};

static struct resource da9063_hwmon_resources[] = {
	{
		.start	= DA9063_IRQ_ADC_RDY,
		.end	= DA9063_IRQ_ADC_RDY,
		.flags	= IORESOURCE_IRQ,
	},
};

static struct resource da9063_gpio_resources[] = {
	{
		.start	= DA9063_IRQ_GPI0,
		.end	= DA9063_IRQ_GPI0,
		.flags	= IORESOURCE_IRQ,
	},
	{
		.start	= DA9063_IRQ_GPI1,
		.end	= DA9063_IRQ_GPI1,
		.flags	= IORESOURCE_IRQ,
	},
	{
		.start	= DA9063_IRQ_GPI2,
		.end	= DA9063_IRQ_GPI2,
		.flags	= IORESOURCE_IRQ,
	},
	{
		.start	= DA9063_IRQ_GPI3,
		.end	= DA9063_IRQ_GPI3,
		.flags	= IORESOURCE_IRQ,
	},
	{
		.start	= DA9063_IRQ_GPI4,
		.end	= DA9063_IRQ_GPI4,
		.flags	= IORESOURCE_IRQ,
	},
	{
		.start	= DA9063_IRQ_GPI5,
		.end	= DA9063_IRQ_GPI5,
		.flags	= IORESOURCE_IRQ,
	},
	{
		.start	= DA9063_IRQ_GPI6,
		.end	= DA9063_IRQ_GPI6,
		.flags	= IORESOURCE_IRQ,
	},
	{
		.start	= DA9063_IRQ_GPI7,
		.end	= DA9063_IRQ_GPI7,
		.flags	= IORESOURCE_IRQ,
	},
	{
		.start	= DA9063_IRQ_GPI8,
		.end	= DA9063_IRQ_GPI8,
		.flags	= IORESOURCE_IRQ,
	},
	{
		.start	= DA9063_IRQ_GPI9,
		.end	= DA9063_IRQ_GPI9,
		.flags	= IORESOURCE_IRQ,
	},
	{
		.start	= DA9063_IRQ_GPI10,
		.end	= DA9063_IRQ_GPI10,
		.flags	= IORESOURCE_IRQ,
	},
	{
		.start	= DA9063_IRQ_GPI11,
		.end	= DA9063_IRQ_GPI11,
		.flags	= IORESOURCE_IRQ,
	},
	{
		.start	= DA9063_IRQ_GPI12,
		.end	= DA9063_IRQ_GPI12,
		.flags	= IORESOURCE_IRQ,
	},
	{
		.start	= DA9063_IRQ_GPI13,
		.end	= DA9063_IRQ_GPI13,
		.flags	= IORESOURCE_IRQ,
	},
	{
		.start	= DA9063_IRQ_GPI14,
		.end	= DA9063_IRQ_GPI14,
		.flags	= IORESOURCE_IRQ,
	},
	{
		.start	= DA9063_IRQ_GPI15,
		.end	= DA9063_IRQ_GPI15,
		.flags	= IORESOURCE_IRQ,
	},
};

static struct resource da9063_comparator_resources[] = {
	{
		.name	= "COMP_1V2",
		.start	= DA9063_IRQ_COMP_1V2,
		.end	= DA9063_IRQ_COMP_1V2,
		.flags	= IORESOURCE_IRQ,
	},
};

static const struct mfd_cell da9063_common_devs[] = {
	{
		.name		= DA9063_DRVNAME_REGULATORS,
		.num_resources	= ARRAY_SIZE(da9063_regulators_resources),
		.resources	= da9063_regulators_resources,
		.of_compatible	= "dlg,da9063-regulators",
	},
	{
		.name		= DA9063_DRVNAME_LEDS,
	},
	{
		.name		= DA9063_DRVNAME_WATCHDOG,
		.of_compatible	= "dlg,da9063-watchdog",
	},
	{
		.name		= DA9063_DRVNAME_HWMON,
		.num_resources	= ARRAY_SIZE(da9063_hwmon_resources),
		.resources	= da9063_hwmon_resources,
		.of_compatible	= "dlg,da9063-hwmon",
	},
	{
		.name		= DA9063_DRVNAME_ONKEY,
		.num_resources	= ARRAY_SIZE(da9063_onkey_resources),
		.resources	= da9063_onkey_resources,
		.of_compatible	= "dlg,da9063-onkey",
	},
	{
		.name		= DA9063_DRVNAME_VIBRATION,
		.of_compatible	= "dlg,da9063-vibration",
	},
};

/* Only present on DA9063 , not on DA9063L */
static const struct mfd_cell da9063_devs[] = {
	{
		.name		= DA9063_DRVNAME_RTC,
		.num_resources	= ARRAY_SIZE(da9063_rtc_resources),
		.resources	= da9063_rtc_resources,
		.of_compatible	= "dlg,da9063-rtc",
	},
	{
		.name		= DA9063_DRVNAME_GPIO,
		.num_resources	= ARRAY_SIZE(da9063_gpio_resources),
		.resources	= da9063_gpio_resources,
		.of_compatible  = "dlg,da9063-gpio",
	},
	{
		.name		= DA9063_DRVNAME_COMPARATOR,
		.num_resources	= ARRAY_SIZE(da9063_comparator_resources),
		.resources	= da9063_comparator_resources,
		.of_compatible	= "dlg,da9063-comparator",
	},
};

static int da9063_clear_fault_log(struct da9063 *da9063)
{
	int ret = 0;
	int fault_log = 0;

	ret = regmap_read(da9063->regmap, DA9063_REG_FAULT_LOG, &fault_log);
	if (ret < 0) {
		dev_err(da9063->dev, "Cannot read FAULT_LOG.\n");
		return -EIO;
	}

	if (fault_log) {
		if (fault_log & DA9063_TWD_ERROR)
			dev_dbg(da9063->dev,
				"Fault log entry detected: DA9063_TWD_ERROR\n");
		if (fault_log & DA9063_POR)
			dev_dbg(da9063->dev,
				"Fault log entry detected: DA9063_POR\n");
		if (fault_log & DA9063_VDD_FAULT)
			dev_dbg(da9063->dev,
				"Fault log entry detected: DA9063_VDD_FAULT\n");
		if (fault_log & DA9063_VDD_START)
			dev_dbg(da9063->dev,
				"Fault log entry detected: DA9063_VDD_START\n");
		if (fault_log & DA9063_TEMP_CRIT)
			dev_dbg(da9063->dev,
				"Fault log entry detected: DA9063_TEMP_CRIT\n");
		if (fault_log & DA9063_KEY_RESET)
			dev_dbg(da9063->dev,
				"Fault log entry detected: DA9063_KEY_RESET\n");
		if (fault_log & DA9063_NSHUTDOWN)
			dev_dbg(da9063->dev,
				"Fault log entry detected: DA9063_NSHUTDOWN\n");
		if (fault_log & DA9063_WAIT_SHUT)
			dev_dbg(da9063->dev,
				"Fault log entry detected: DA9063_WAIT_SHUT\n");
	}

	ret = regmap_write(da9063->regmap,
			   DA9063_REG_FAULT_LOG,
			   fault_log);
	if (ret < 0)
		dev_err(da9063->dev,
			"Cannot reset FAULT_LOG values %d\n", ret);

	return ret;
}

int da9063_dump(struct da9063 *da9063)
{
	int j = 0;
	int i, ret;
	int err_cnt = 0;
	int inv_cnt = 0;
	unsigned int val[16];
	unsigned int invalid = 0;
	unsigned int reg_second_d = (da9063->variant_code ==  PMIC_DA9063_AD) ?
			DA9063_AD_REG_SECOND_D : DA9063_BB_REG_SECOND_D;
	unsigned int reg_gp_id_19 = (da9063->variant_code ==  PMIC_DA9063_AD) ?
			DA9063_AD_REG_GP_ID_19 : DA9063_BB_REG_GP_ID_19;

	if (!da9063)
		return -EINVAL;

	printk(KERN_ALERT"PMIC  00 01 02 03 04 05 06 07  08 09 0a 0b 0c 0d 0e 0f  err  inv\n"
			 "      -----------------------  -----------------------  ---  ---\n");
	for (i = DA9063_REG_PAGE_CON; i <= DA9063_REG_CONFIG_ID; i++) {
		/* Check for invalid registers */
		if ((i > reg_second_d && i < (DA9063_REG_SEQ - 1)) ||
		    (i > DA9063_REG_AUTO3_LOW && i < (DA9063_REG_OTP_CONT - 1)) ||
		    (i > reg_gp_id_19 && i < (DA9063_REG_DEVICE_ID - 1))) {
			invalid = 1;
		}

		if (!invalid) {
			ret = regmap_read(da9063->regmap, i, &val[j]);
			if (ret < 0) {
				val[j] = 0xee;
				err_cnt++;
			}
		} else {
			val[j] = 0xff;
			inv_cnt++;
			invalid = 0;
		}

		if (j == 15) {
			printk(KERN_ALERT"%04x  %02x %02x %02x %02x %02x %02x %02x %02x"
					     "  %02x %02x %02x %02x %02x %02x %02x %02x  %3d  %3d\n",
					     i-15, val[0], val[1], val[2], val[3], val[4], val[5],
					     val[6], val[7], val[8], val[9], val[10], val[11],
					     val[12], val[13], val[14], val[15],
					     err_cnt, inv_cnt);
			err_cnt = 0;
			inv_cnt = 0;
			j = 0;
		} else {
			j++;
		}
	}
	printk(KERN_ALERT"\n");

	mdelay(500);

	return 0;
}

void da9063_power_off(void)
{
	int i;
	struct device_node *np;
	u32 poweroff_gpos[MAX_GPIOS] = {0};
	u32 poweroff_ldos[MAX_LDOS] = {0};
	u32 wakeup_ldos[MAX_LDOS] = {0};
	u32 wakeup_signals[MAX_SIGNALS];
	u8 mask_reg_val[N_MASK_REGISTERS] = {0xFF, 0xFF, 0xFF, 0xFF};
	int gpos_nbr;
	int ldos_nbr;
	int ret;

	BUG_ON(!da9063_data);

	np = dev_of_node(da9063_data->dev);

	/* Disable GPIOS */
	gpos_nbr = of_property_read_variable_u32_array(np, "poweroff-gpos",
					      poweroff_gpos, 1, MAX_GPIOS);

	if (gpos_nbr > 0) {
		for (i = 0; i < gpos_nbr; i++) {
			int reg = DA9063_REG_GPIO_MODE0_7;
			int mask = DA9063_GPIO0_MODE << poweroff_gpos[i];

			if (poweroff_gpos[i] < 0 || poweroff_gpos[i] > MAX_GPIOS) {
				dev_err(da9063_data->dev,
					"poweroff-gpos not in range : %d\n",
					poweroff_gpos[i]);
				continue;
			}
			if (poweroff_gpos[i] >= 8) {
				reg = DA9063_REG_GPIO_MODE8_15;
				mask = DA9063_GPIO0_MODE << (poweroff_gpos[i] - 8);
			}
			dev_info(da9063_data->dev, "Disabling GPIO%d\n", poweroff_gpos[i]);
			regmap_update_bits(da9063_data->regmap, reg, mask, 0);
		}
	} else if (gpos_nbr == -EINVAL || gpos_nbr == -ENODATA) {
		dev_dbg(da9063_data->dev, "No GPIO defined for poweroff operation\n");
	} else if (gpos_nbr == -EOVERFLOW) {
		dev_err(da9063_data->dev, "Too many poweroff-gpos defined\n");
	}

	/* Disable LDOs */
	ldos_nbr = of_property_read_variable_u32_array(np, "poweroff-ldos",
						poweroff_ldos, 1, MAX_LDOS);

	if (ldos_nbr > 0) {
		for (i = 0; i < ldos_nbr; i++) {
			int reg = DA9063_REG_LDO1_CONT - 1 + poweroff_ldos[i];

			if (poweroff_ldos[i] < 0 || poweroff_ldos[i] > MAX_LDOS) {
				dev_err(da9063_data->dev,
					"poweroff-ldos not in range : %d\n",
					poweroff_ldos[i]);
				continue;
			}
			dev_info(da9063_data->dev, "Disabling LDO%d\n", poweroff_ldos[i]);
			regmap_update_bits(da9063_data->regmap, reg, DA9063_LDO_EN, 0);
		}
	} else if (ldos_nbr == -EINVAL || ldos_nbr == -ENODATA) {
		dev_dbg(da9063_data->dev, "No LDO defined for poweroff operation\n");
	} else if (ldos_nbr == -EOVERFLOW) {
		dev_err(da9063_data->dev, "Too many poweroff-ldos defined\n");
	}


	/* Enable WakeUP LDO */
	ldos_nbr = of_property_read_variable_u32_array(np, "wakeup-ldos",
						       wakeup_ldos, 1, MAX_LDOS);

	if (ldos_nbr > 0) {
		for (i = 0; i < ldos_nbr; i++) {
			int reg = DA9063_REG_LDO1_CONT - 1 + wakeup_ldos[i];

			if (wakeup_ldos[i] < 0 || wakeup_ldos[i] > MAX_LDOS) {
				dev_err(da9063_data->dev,
					"wakeup-ldos not in range : %d\n",
					wakeup_ldos[i]);
				continue;
			}
			dev_info(da9063_data->dev, "Keeping LDO%d for wakeup\n", wakeup_ldos[i]);
			regmap_update_bits(da9063_data->regmap, reg,
				               DA9063_LDO_CONF, DA9063_LDO_CONF);
		}
	} else if (ldos_nbr == -EINVAL || ldos_nbr == -ENODATA) {
		dev_dbg(da9063_data->dev, "No wakeup LDO defined for poweroff operation\n");
	} else if (ldos_nbr == -EOVERFLOW) {
		dev_err(da9063_data->dev, "Too many wakeup-ldos defined\n");
	}

	ret = of_property_read_variable_u32_array(np, "wakeup-signals",
		wakeup_signals, 1, MAX_SIGNALS);

	if (ret > 0) {
		for (i = 0; i < ret; i++) {
			u8 mask;
			u8 reg;
			u8 bitshift = wakeup_signals[i];
			if (bitshift >= DA9063_REG_IRQ_MASK_A_RESERVED_BITS_OFFSET) {
				bitshift += DA9063_REG_IRQ_MASK_A_N_RESERVED_BITS;
			}
			reg = bitshift / 8;
			mask = 1 << (bitshift % 8);
			mask_reg_val[reg] &= ~(mask);
			dev_info(da9063_data->dev, "Allow IRQ%d for wakeup.\n",
				wakeup_signals[i]);
		}
		for (i = 0; i < N_MASK_REGISTERS; i++) {
			regmap_update_bits(da9063_data->regmap, DA9063_REG_IRQ_MASK_A + i,
				0xFF, mask_reg_val[i]);
		}
	} else if (ret == -EINVAL || ret == -ENODATA) {
		dev_dbg(da9063_data->dev, "No wakeup-signals defined for poweroff operation. IRQ mask is unchanged.\n");
	} else if (ret == -EOVERFLOW) {
		dev_err(da9063_data->dev, "Too many irq-shutdown-signals defined. IRQ mask is unchanged.\n");
	}

	/* Power down */
	regmap_update_bits(da9063_data->regmap, DA9063_REG_CONTROL_A,
			   DA9063_SYSTEM_EN, 0);

	// Do not unlock mutex to avoid further accesses
	// Do not return
	while (1);
}

int da9063_device_init(struct da9063 *da9063, unsigned int irq)
{
	int ret;
	int t_offset;

	ret = da9063_clear_fault_log(da9063);
	if (ret < 0)
		dev_err(da9063->dev, "Cannot clear fault log\n");

	da9063->flags = 0;
	da9063->irq_base = -1;
	da9063->chip_irq = irq;

	ret = da9063_irq_init(da9063);
	if (ret) {
		dev_err(da9063->dev, "Cannot initialize interrupts.\n");
		return ret;
	}

	da9063->irq_base = regmap_irq_chip_get_base(da9063->regmap_irq);

	ret = regmap_read(da9063->regmap, DA9063_REG_T_OFFSET, &t_offset);
	if (ret < 0) {
		dev_err(da9063->dev, "Cannot read chip temperature offset.\n");
		return -EIO;
	}
	da9063->t_offset = (s8) t_offset;

	ret = devm_mfd_add_devices(da9063->dev, PLATFORM_DEVID_NONE,
				   da9063_common_devs,
				   ARRAY_SIZE(da9063_common_devs),
				   NULL, da9063->irq_base,
				   regmap_irq_get_domain(da9063->regmap_irq));
	if (ret) {
		dev_err(da9063->dev, "Failed to add child devices\n");
		return ret;
	}

	if (da9063->type == PMIC_TYPE_DA9063) {
		ret = devm_mfd_add_devices(da9063->dev, PLATFORM_DEVID_NONE,
					   da9063_devs, ARRAY_SIZE(da9063_devs),
					   NULL, da9063->irq_base,
					   regmap_irq_get_domain(da9063->regmap_irq));
		if (ret) {
			dev_err(da9063->dev, "Failed to add child devices\n");
			return ret;
		}
	}

	da9063_data = da9063;

	pm_power_off = da9063_power_off;

	return ret;
}

MODULE_DESCRIPTION("PMIC driver for Dialog DA9063");
MODULE_AUTHOR("Krystian Garbaciak");
MODULE_AUTHOR("Michal Hajduk");
MODULE_LICENSE("GPL");
