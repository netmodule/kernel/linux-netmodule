/*
 * nm-fpga-gpio18.c - NetModule FPGA GPIO driver for NBHW18
 *
 * Copyright (C) 2018 NetModule AG
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/export.h>
#include <linux/bug.h>
#include <linux/kernel.h>

#define NM_FPGA_GPIO_NO_INTERRUPTS
#include "nm-fpga-gpio-conf.h"
#include <dt-bindings/gpio/nm-nbhw18-fpga-gpio.h>

#define NM_NBHW18_FPGA_REG_OUTPUT1           0x0008

#define NM_NBHW18_FPGA_REG_LED               0x0020

#define NM_NBHW18_FPGA_REG_SERIAL_CONTROL    0x0024

#define NM_NBHW18_FPGA_REG_PCIE_RESET        0x0030
#define NM_NBHW18_FPGA_REG_PCIE_POWER        0x0032

#define NM_NBHW18_FPGA_REG_SFP_MONITOR       0x0074
#define NM_NBHW18_FPGA_REG_SFP_CTRL          0x0076

#define NM_NBHW18_FPGA_REG_PCIE_1_SLOT_DIR1  0x0100
#define NM_NBHW18_FPGA_REG_PCIE_1_SLOT_CTRL1 0x0102

#define NM_NBHW18_FPGA_REG_PCIE_2_SLOT_DIR1  0x0200
#define NM_NBHW18_FPGA_REG_PCIE_2_SLOT_CTRL1 0x0202

#define NM_NBHW18_FPGA_REG_EXT_SLOT_DIR         0x0300
#define NM_NBHW18_FPGA_REG_EXT_SLOT_CTRL1       0x0302

#define NM_NBHW18_FPGA_REG_LOW_EXT_SLOT_1_DIR   0x0400
#define NM_NBHW18_FPGA_REG_LOW_EXT_SLOT_1_CTRL1 0x0402

#define NM_NBHW18_FPGA_REG_LOW_EXT_SLOT_2_DIR   0x0500
#define NM_NBHW18_FPGA_REG_LOW_EXT_SLOT_2_CTRL1 0x0502

static struct nm_fpga_gpio_reg_init reg_18[] = {
	{NM_NBHW18_FPGA_REG_LED, 0x0001},

	/* Write to control registers first (to avoid unnecessary switching of
	 * output signal, search for "FPGA IO direction change semantics" in
	 * nm-fpga-gpio.c). */
	{NM_NBHW18_FPGA_REG_PCIE_1_SLOT_CTRL1, 0x0020},
	{NM_NBHW18_FPGA_REG_PCIE_1_SLOT_DIR1,  0x0000},

	{NM_NBHW18_FPGA_REG_PCIE_2_SLOT_CTRL1, 0x0020},
	{NM_NBHW18_FPGA_REG_PCIE_2_SLOT_DIR1,  0x0000},

	{NM_NBHW18_FPGA_REG_EXT_SLOT_CTRL1,       0x0020},
	{NM_NBHW18_FPGA_REG_EXT_SLOT_DIR,         0x0000},

	{NM_NBHW18_FPGA_REG_LOW_EXT_SLOT_1_CTRL1, 0x0020},
	{NM_NBHW18_FPGA_REG_LOW_EXT_SLOT_1_DIR,   0x0000},

	{NM_NBHW18_FPGA_REG_LOW_EXT_SLOT_2_CTRL1, 0x0020},
	{NM_NBHW18_FPGA_REG_LOW_EXT_SLOT_2_DIR,   0x0000},
};

static struct nm_fpga_gpio_line line_18[] = {
	[NM_NBHW18_FPGA_GPIO_LED0_GREEN] = {
		.reg = NM_NBHW18_FPGA_REG_LED,
		.bit = 0,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_LED0_RED] = {
		.reg = NM_NBHW18_FPGA_REG_LED,
		.bit = 1,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_LED1_GREEN] = {
		.reg = NM_NBHW18_FPGA_REG_LED,
		.bit = 2,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_LED1_RED] = {
		.reg = NM_NBHW18_FPGA_REG_LED,
		.bit = 3,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_LED2_GREEN] = {
		.reg = NM_NBHW18_FPGA_REG_LED,
		.bit = 4,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_LED2_RED] = {
		.reg = NM_NBHW18_FPGA_REG_LED,
		.bit = 5,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_LED3_GREEN] = {
		.reg = NM_NBHW18_FPGA_REG_LED,
		.bit = 6,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_LED3_RED] = {
		.reg = NM_NBHW18_FPGA_REG_LED,
		.bit = 7,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_LED4_GREEN] = {
		.reg = NM_NBHW18_FPGA_REG_LED,
		.bit = 8,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_LED4_RED] = {
		.reg = NM_NBHW18_FPGA_REG_LED,
		.bit = 9,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_LED5_GREEN] = {
		.reg = NM_NBHW18_FPGA_REG_LED,
		.bit = 10,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_LED5_RED] = {
		.reg = NM_NBHW18_FPGA_REG_LED,
		.bit = 11,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},

	[NM_NBHW18_FPGA_GPIO_PCIE1_RST] = {
		.reg = NM_NBHW18_FPGA_REG_PCIE_RESET,
		.bit = 0,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_PCIE2_RST] = {
		.reg = NM_NBHW18_FPGA_REG_PCIE_RESET,
		.bit = 1,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_EXT_SLOT_RST] = {
		.reg = NM_NBHW18_FPGA_REG_PCIE_RESET,
		.bit = 2,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_LOW_EXT_SLOT_1_RST] = {
		.reg = NM_NBHW18_FPGA_REG_PCIE_RESET,
		.bit = 3,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_LOW_EXT_SLOT_2_RST] = {
		.reg = NM_NBHW18_FPGA_REG_PCIE_RESET,
		.bit = 4,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_SFP_RST] = {
		.reg = NM_NBHW18_FPGA_REG_PCIE_RESET,
		.bit = 5,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},

	[NM_NBHW18_FPGA_GPIO_PCIE1_PWR] = {
		.reg = NM_NBHW18_FPGA_REG_PCIE_POWER,
		.bit = 0,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_PCIE2_PWR] = {
		.reg = NM_NBHW18_FPGA_REG_PCIE_POWER,
		.bit = 1,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_EXT_SLOT_PWR] = {
		.reg = NM_NBHW18_FPGA_REG_PCIE_POWER,
		.bit = 2,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_LOW_EXT_SLOT_1_PWR] = {
		.reg = NM_NBHW18_FPGA_REG_PCIE_POWER,
		.bit = 3,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_LOW_EXT_SLOT_2_PWR] = {
		.reg = NM_NBHW18_FPGA_REG_PCIE_POWER,
		.bit = 4,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_SFP_PWR] = {
		.reg = NM_NBHW18_FPGA_REG_PCIE_POWER,
		.bit = 5,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},

	[NM_NBHW18_FPGA_GPIO_PCIE1_WDIS] = {
		.reg = NM_NBHW18_FPGA_REG_PCIE_1_SLOT_CTRL1,
		.bit = 5,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_PCIE2_WDIS] = {
		.reg = NM_NBHW18_FPGA_REG_PCIE_2_SLOT_CTRL1,
		.bit = 5,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_EXT_SLOT_WDIS] = {
		.reg = NM_NBHW18_FPGA_REG_EXT_SLOT_CTRL1,
		.bit = 5,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_LOW_EXT_SLOT_1_WDIS] = {
		.reg = NM_NBHW18_FPGA_REG_LOW_EXT_SLOT_1_CTRL1,
		.bit = 5,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_LOW_EXT_SLOT_2_WDIS] = {
		.reg = NM_NBHW18_FPGA_REG_LOW_EXT_SLOT_2_CTRL1,
		.bit = 5,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},

	[NM_NBHW18_FPGA_GPIO_EN_5VO_INT] = {
		.reg = NM_NBHW18_FPGA_REG_OUTPUT1,
		.bit = 0,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_SD_EN] = {
		.reg = NM_NBHW18_FPGA_REG_OUTPUT1,
		.bit = 1,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_GPS_PWR_EN] = {
		.reg = NM_NBHW18_FPGA_REG_OUTPUT1,
		.bit = 2,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},

	[NM_NBHW18_FPGA_GPIO_PCIE1_WAKE] = {
		.reg = NM_NBHW18_FPGA_REG_PCIE_1_SLOT_CTRL1,
		.bit = 4,
		.flags = NM_GPIO_FLAG_READ_WRITE,
		.has_IO_direction_control = 1,
	},
	[NM_NBHW18_FPGA_GPIO_PCIE2_WAKE] = {
		.reg = NM_NBHW18_FPGA_REG_PCIE_2_SLOT_CTRL1,
		.bit = 4,
		.flags = NM_GPIO_FLAG_READ_WRITE,
		.has_IO_direction_control = 1,
	},
	[NM_NBHW18_FPGA_GPIO_EXT_SLOT_WAKE] = {
		.reg = NM_NBHW18_FPGA_REG_EXT_SLOT_CTRL1,
		.bit = 4,
		.flags = NM_GPIO_FLAG_READ_WRITE,
		.has_IO_direction_control = 1,
	},
	[NM_NBHW18_FPGA_GPIO_LOW_EXT_SLOT_1_WAKE] = {
		.reg = NM_NBHW18_FPGA_REG_LOW_EXT_SLOT_1_CTRL1,
		.bit = 4,
		.flags = NM_GPIO_FLAG_READ_WRITE,
		.has_IO_direction_control = 1,
	},
	[NM_NBHW18_FPGA_GPIO_LOW_EXT_SLOT_2_WAKE] = {
		.reg = NM_NBHW18_FPGA_REG_LOW_EXT_SLOT_2_CTRL1,
		.bit = 4,
		.flags = NM_GPIO_FLAG_READ_WRITE,
		.has_IO_direction_control = 1,
	},

	[NM_NBHW18_FPGA_GPIO__LOS] = {
		.reg = NM_NBHW18_FPGA_REG_SFP_MONITOR,
		.bit = 0,
		.flags = NM_GPIO_FLAG_READ,
	},
	[NM_NBHW18_FPGA_GPIO_PRSNT] = {
		.reg = NM_NBHW18_FPGA_REG_SFP_MONITOR,
		.bit = 1,
		.flags = NM_GPIO_FLAG_READ,
	},
	[NM_NBHW18_FPGA_GPIO_TX_FAULT] = {
		.reg = NM_NBHW18_FPGA_REG_SFP_MONITOR,
		.bit = 2,
		.flags = NM_GPIO_FLAG_READ,
	},
	[NM_NBHW18_FPGA_GPIO_PWR_FAULT] = {
		.reg = NM_NBHW18_FPGA_REG_SFP_MONITOR,
		.bit = 3,
		.flags = NM_GPIO_FLAG_READ,
	},
	[NM_NBHW18_FPGA_GPIO_SFP_TX_DISABLE] = {
		.reg = NM_NBHW18_FPGA_REG_SFP_CTRL,
		.bit = 0,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},

	[NM_NBHW18_FPGA_GPIO_RS232_ENABLE] = {
		.reg = NM_NBHW18_FPGA_REG_SERIAL_CONTROL,
		.bit = 0,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_RS485_ENABLE] = {
		.reg = NM_NBHW18_FPGA_REG_SERIAL_CONTROL,
		.bit = 1,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
	[NM_NBHW18_FPGA_GPIO_RS485_TERM_ENABLE] = {
		.reg = NM_NBHW18_FPGA_REG_SERIAL_CONTROL,
		.bit = 2,
		.flags = NM_GPIO_FLAG_READ_WRITE,
	},
};

DEFINE_GPIO_CONF(18);
