/*
 * nm-fpga-gpio.c - NetModule FPGA GPIO driver
 *
 * Copyright (C) 2018 NetModule AG
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/of_device.h>

#include <linux/regmap.h>
#include <linux/gpio/driver.h>
#include <linux/interrupt.h>

#include "nm-fpga.h"
#include "nm-fpga-gpio.h"

struct nm_fpga_gpio {
	struct nm_fpga* fpga;
	const struct nm_fpga_gpio_config* conf;
	struct gpio_chip gpio_chip;
	struct irq_chip irq_chip;
};

static inline struct nm_fpga_gpio* nm_gpio_get_ctx(struct gpio_chip *gc)
{
	/* return container_of(gc, struct nm_fpga_gpio, gpio_chip); */
	return gpiochip_get_data(gc);
}

static inline struct nm_fpga_gpio* nm_irq_get_ctx(struct irq_data *data)
{
	/* return container_of(data->chip, struct nm_fpga_gpio, irq_chip); */
	return gpiochip_get_data(irq_data_get_irq_chip_data(data));
}

static int nm_fpga_gpio_get(struct gpio_chip *gc, unsigned offset)
{
	struct nm_fpga_gpio *gpio = nm_gpio_get_ctx(gc);
	unsigned val;
	BUG_ON(!gpio->conf->is_valid(offset));
	BUG_ON(!gpio->conf->can_read(offset));
	regmap_read(gpio->fpga->regmap, gpio->conf->get_reg(offset), &val);
	val = (val >> gpio->conf->get_bit(offset)) & 1;
	return val;
}

static void nm_fpga_gpio_set(struct gpio_chip *gc, unsigned offset, int value)
{
	struct nm_fpga_gpio *gpio = nm_gpio_get_ctx(gc);
	unsigned reg, bit, mask;
	BUG_ON(!gpio->conf->is_valid(offset));
	BUG_ON(!gpio->conf->can_write(offset));
	reg = gpio->conf->get_reg(offset);
	bit = gpio->conf->get_bit(offset);
	mask = 1u << bit;
	if (value)
		value = mask;
	regmap_update_bits(gpio->fpga->regmap, reg, mask, value);
}

static int nm_fpga_gpio_dir_out(struct gpio_chip *gc, unsigned offset, int value)
{
	struct nm_fpga_gpio *gpio = nm_gpio_get_ctx(gc);
	unsigned reg, bit, mask;
	BUG_ON(!gpio->conf->is_valid(offset));
	if (!gpio->conf->can_write(offset))
		return -EINVAL; /* writes to read-only bits are ignored */
	reg = gpio->conf->get_reg(offset);
	bit = gpio->conf->get_bit(offset);
	mask = 1u << bit;
	if (value)
		value = mask;
	/* FPGA IO direction change semantics:
	 * If the value is set as input, the written value on ctrl output
	 * register will be stored but hidden from the user as long as it is an
	 * input (if you read back the register you will read the pin value). As
	 * soon as you set a bit to output, it will apply the latest value you
	 * have put on the register (this time you will be able to read back
	 * what you write on this bit).
	 */
	/* Write GPIO value first (to avoid unnecessary switching of output
	 * signal, see "FPGA IO direction change semantics" above): */
	regmap_update_bits(gpio->fpga->regmap, reg, mask, value);
	if (!gpio->conf->has_IO_direction_control(offset))
		return 0;
	/* Set output IO direction: */
	reg = gpio->conf->get_IO_direction_reg(offset);
	mask = 1u << gpio->conf->get_IO_direction_bit(offset);
	regmap_update_bits(gpio->fpga->regmap, reg, mask, mask);
	return 0;
}

static int nm_fpga_gpio_dir_in(struct gpio_chip *gc, unsigned offset)
{
	struct nm_fpga_gpio *gpio = nm_gpio_get_ctx(gc);
	unsigned mask, reg;
	BUG_ON(!gpio->conf->is_valid(offset));
	if (!gpio->conf->can_read(offset))
		return -EINVAL; /* reads from write-only bits always return 0 */
	if (!gpio->conf->has_IO_direction_control(offset))
		return 0;
	/* Set input IO direction: */
	mask = 1u << gpio->conf->get_IO_direction_bit(offset);
	reg = gpio->conf->get_IO_direction_reg(offset);
	regmap_update_bits(gpio->fpga->regmap, reg, mask, 0);
	return 0;
}

static const struct of_device_id nm_fpga_gpio_of_match[] = {
	{
		.compatible = "netmodule,nbhw08-fpga-gpio",
		.data = &nm_fpga_gpio_08,
	},
	{
		.compatible = "netmodule,nbhw12-fpga-gpio",
		.data = &nm_fpga_gpio_12,
	},
	{
		.compatible = "netmodule,nbhw14-fpga-gpio",
		.data = &nm_fpga_gpio_14,
	},
	{
		.compatible = "netmodule,nbhw17-fpga-gpio",
		.data = &nm_fpga_gpio_17,
	},
	{
		.compatible = "netmodule,nbhw18-fpga-gpio",
		.data = &nm_fpga_gpio_18,
	},
	{ },
};
MODULE_DEVICE_TABLE(of, nm_fpga_gpio_of_match);

static void nm_fpga_gpio_irq_ack(struct irq_data *data)
{
	struct nm_fpga_gpio *gpio = nm_irq_get_ctx(data);
	unsigned offset = irqd_to_hwirq(data);
	unsigned irq_reg_idx = gpio->conf->get_irq_reg_idx(offset);
	u16 reg_ack = gpio->conf->get_IntAck_reg(irq_reg_idx);
	unsigned value = 1u << gpio->conf->get_irq_reg_bit(offset);
	regmap_write(gpio->fpga->regmap, reg_ack, value);
}

static void set_irq_mask_bit(struct irq_data *data, unsigned value)
{
	struct nm_fpga_gpio *gpio = nm_irq_get_ctx(data);
	unsigned offset = irqd_to_hwirq(data);
	unsigned irq_reg_idx = gpio->conf->get_irq_reg_idx(offset);
	u16 reg_mask = gpio->conf->get_IntMask_reg(irq_reg_idx);
	u16 mask = 1u << gpio->conf->get_irq_reg_bit(offset);
	value <<= gpio->conf->get_irq_reg_bit(offset);
	regmap_update_bits(gpio->fpga->regmap, reg_mask, mask, value);
}

static void nm_fpga_gpio_irq_mask(struct irq_data *data)
{
	set_irq_mask_bit(data, 1);
}

static void nm_fpga_gpio_irq_unmask(struct irq_data *data)
{
	set_irq_mask_bit(data, 0);
}

static int nm_fpga_gpio_irq_set_type(struct irq_data *data, unsigned flow_type)
{
	struct nm_fpga_gpio *gpio = nm_irq_get_ctx(data);
	unsigned offset = irqd_to_hwirq(data);

	/* We've already masked out all GPIOs that don't have IRQs: */
	BUG_ON(!gpio->conf->has_irq(offset));

	if (flow_type != IRQ_TYPE_EDGE_BOTH)
		return -EINVAL;
	return 0;
}

static irqreturn_t nm_fpga_irq_handler(int irq, void *dev_id)
{
	struct nm_fpga_gpio *gpio = (struct nm_fpga_gpio*)dev_id;
	unsigned num_irq_regs = gpio->conf->get_num_irq_regs();
	struct irq_domain *irqdomain = gpio->gpio_chip.irq.domain;
	unsigned reg;
	irqreturn_t ret = IRQ_NONE;
	for (reg = 0; reg < num_irq_regs; reg++) {
		u16 reg_pending = gpio->conf->get_IntPending_reg(reg);
		unsigned long pending;
		unsigned tmp;
		unsigned bit;
		regmap_read(gpio->fpga->regmap, reg_pending, &tmp);
		pending = tmp;
		/* filter out IRQs we don't handle: */
		pending &= gpio->conf->get_irq_mask(reg);
		if (!pending)
			continue; /* minor performance optimization */
		ret = IRQ_HANDLED;
		for_each_set_bit(bit, &pending, 16) {
			/* we need to route this IRQ to GPIO subsystem */
			unsigned offset = gpio->conf->get_offset(reg, bit);
			int gpio_irq = irq_find_mapping(irqdomain, offset);
			generic_handle_irq(gpio_irq);
		}
	}
	return ret;
}

static int nm_fpga_init_valid_mask(struct gpio_chip *chip,
                        unsigned long *valid_mask,
                        unsigned int ngpios)
{
	struct nm_fpga_gpio *gpio = nm_gpio_get_ctx(chip);
	int offset;
	for (offset = 0; offset < ngpios; ++offset) {
		if (!gpio->conf->is_valid(offset))
			clear_bit(offset, valid_mask);
	}

	return 0;
}

static void nm_fpga_init_irq_valid_mask(struct gpio_chip *chip,
                            unsigned long *valid_mask,
                            unsigned int ngpios)
{
	struct nm_fpga_gpio *gpio = nm_gpio_get_ctx(chip);
	int offset;
	for (offset = 0; offset < ngpios; ++offset) {
		if (!gpio->conf->has_irq(offset))
			clear_bit(offset, valid_mask);
	}
}

static int nm_fpga_gpio_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct nm_fpga_gpio *gpio;
	const struct of_device_id *of_id;
	int ret;

	gpio = devm_kzalloc(dev, sizeof(*gpio), GFP_KERNEL);
	if (!gpio)
		return -ENOMEM;
	gpio->fpga = dev_get_drvdata(dev->parent);
	of_id = of_match_device(of_match_ptr(nm_fpga_gpio_of_match), dev);
	if (!of_id)
		return -EINVAL;
	gpio->conf = of_id->data;

	gpio->conf->init_fpga(gpio->fpga->regmap);

	gpio->gpio_chip.label = "nm-fpga-gpio";
	gpio->gpio_chip.parent = dev;
	gpio->gpio_chip.owner = THIS_MODULE;
	gpio->gpio_chip.of_node = dev->of_node;
	gpio->gpio_chip.direction_input = nm_fpga_gpio_dir_in;
	gpio->gpio_chip.direction_output = nm_fpga_gpio_dir_out;
	gpio->gpio_chip.get = nm_fpga_gpio_get;
	gpio->gpio_chip.set = nm_fpga_gpio_set;
	gpio->gpio_chip.ngpio = gpio->conf->get_num_gpios();
	gpio->gpio_chip.init_valid_mask = nm_fpga_init_valid_mask;
	gpio->gpio_chip.irq.init_valid_mask = nm_fpga_init_irq_valid_mask;
	/* TODO set base to -1 (using non-negative values is deprecated) as soon
	 * as we get rid of old-style global gpio numbering. */
	gpio->gpio_chip.base = gpio->conf->get_gpio_chip_base();

	ret = devm_gpiochip_add_data(dev, &gpio->gpio_chip, gpio);
	if (ret) {
		dev_err(dev, "cannot register gpio_chip : %d\n", ret);
		return ret;
	}

	if (gpio->conf->get_num_irq_regs()) {
		gpio->irq_chip.name = "nm-fpga-gpio";
		gpio->irq_chip.irq_ack = nm_fpga_gpio_irq_ack;
		gpio->irq_chip.irq_mask = nm_fpga_gpio_irq_mask;
		gpio->irq_chip.irq_unmask = nm_fpga_gpio_irq_unmask;
		gpio->irq_chip.irq_set_type = nm_fpga_gpio_irq_set_type;

		ret = gpiochip_irqchip_add(&gpio->gpio_chip, &gpio->irq_chip, 0,
		                           handle_level_irq, IRQ_TYPE_NONE);
		if (ret) {
			dev_err(dev, "cannot add gpio irq chip\n");
			return ret;
		}

		/* Note: this interrupt is also used by rdp driver. Search for
		 * "request_irq" in drivers/misc/rdp/rdp_plugin_pcm_interface.c
		 */
		ret = devm_request_irq(dev, gpio->fpga->irq, nm_fpga_irq_handler,
		                       IRQF_SHARED, dev_name(dev), gpio);
		if (ret < 0) {
			dev_err(dev, "cannot request irq\n");
			return ret;
		}
	}

	platform_set_drvdata(pdev, gpio);
	dev_info(dev, "%s completed successfully\n", __FUNCTION__);
	return 0;
}

static struct platform_driver nm_fpga_gpio_driver = {
	.probe = nm_fpga_gpio_probe,
	.driver = {
		.name = "nm-fpga-gpio",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(nm_fpga_gpio_of_match),
	},
};
module_platform_driver(nm_fpga_gpio_driver);

MODULE_DESCRIPTION("NetModule FPGA GPIO driver");
MODULE_AUTHOR("Alexandre Bard");
MODULE_AUTHOR("Viacheslav Volkov");
MODULE_AUTHOR("NetModule AG");
MODULE_LICENSE("GPL");
