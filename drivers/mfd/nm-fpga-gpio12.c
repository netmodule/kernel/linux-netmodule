/*
 * nm-fpga-gpio12.c - NetModule FPGA GPIO driver for NBHW12
 *
 * Copyright (C) 2018 NetModule AG
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/export.h>
#include <linux/bug.h>
#include <linux/kernel.h>

#define NM_FPGA_GPIO_NO_INTERRUPTS
#include "nm-fpga-gpio-conf.h"
#include <dt-bindings/gpio/nm-nbhw12-fpga-gpio.h>

#define NM_NBHW12_FPGA_REG_PCIE_1_SLOT_DIR1  0x0100
#define NM_NBHW12_FPGA_REG_PCIE_1_SLOT_CTRL1 0x0102

#define NM_NBHW12_FPGA_REG_PCIE_2_SLOT_DIR1  0x0200
#define NM_NBHW12_FPGA_REG_PCIE_2_SLOT_CTRL1 0x0202

static struct nm_fpga_gpio_reg_init reg_12[] = {
	/* TODO why set WDIS to output (value: 1)? */
	{NM_NBHW12_FPGA_REG_PCIE_1_SLOT_CTRL1, 0x0020},
	{NM_NBHW12_FPGA_REG_PCIE_1_SLOT_DIR1,  0x0020},

	{NM_NBHW12_FPGA_REG_PCIE_2_SLOT_CTRL1, 0x0020},
	{NM_NBHW12_FPGA_REG_PCIE_2_SLOT_DIR1,  0x0020},
};

static struct nm_fpga_gpio_line line_12[] = {
	[NM_NBHW12_FPGA_GPIO_STS_WAKE_0] = {
		.reg = NM_NBHW12_FPGA_REG_PCIE_1_SLOT_CTRL1,
		.bit = 4,
		.flags = NM_GPIO_FLAG_READ_WRITE,
		.has_IO_direction_control = 1,
	},
	[NM_NBHW12_FPGA_GPIO_STS_WAKE_1] = {
		.reg = NM_NBHW12_FPGA_REG_PCIE_2_SLOT_CTRL1,
		.bit = 4,
		.flags = NM_GPIO_FLAG_READ_WRITE,
		.has_IO_direction_control = 1,
	},
};

DEFINE_GPIO_CONF(12);
