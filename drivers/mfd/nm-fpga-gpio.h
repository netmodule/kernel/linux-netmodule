/*
 * nm-fpga-gpio.h - NetModule FPGA GPIO driver
 *
 * Copyright (C) 2018 NetModule AG
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef __MFD_NM_FPGA_GPIO_H
#define __MFD_NM_FPGA_GPIO_H

#include <linux/types.h>
#include <linux/regmap.h>

struct nm_fpga_gpio_config {
	void (*init_fpga)(struct regmap* regmap);
	unsigned (*get_num_gpios)(void);
	int (*get_gpio_chip_base)(void);

	/* get info about GPIO line with a given offset: */
	u16 (*get_reg)(unsigned offset);
	unsigned (*get_bit)(unsigned offset);
	bool (*can_read)(unsigned offset);
	bool (*can_write)(unsigned offset);
	bool (*has_irq)(unsigned offset);
	bool (*has_IO_direction_control)(unsigned offset);
	u16 (*get_IO_direction_reg)(unsigned offset);
	unsigned (*get_IO_direction_bit)(unsigned offset);
	/* Currently our FPGA GPIO range has "gaps" (not all GPIO offsets are
	 * valid). Since it is practically impossible to switch all drivers to
	 * new GPIO DT bindings in one step, we will preserve GPIO numbering
	 * (and GPIO "gaps") for now. We will step by step replace hardcoded
	 * GPIO numbers with constants defined in files like:
	 * include/dt-bindings/gpio/nm-nbhw12-fpga-gpio.h
	 * The very same constants will be used in DT for new drivers. Therefore
	 * in the end we will be able easily and safely reorder FPGA GPIO
	 * numbers and get rid of GPIO "gaps". The following function will be
	 * used to check whether given GPIO offset is valid. */
	bool (*is_valid)(unsigned offset);

	/* The following function returns number of IRQ registers we handle:
	 * 0 if there are no interrupts routed to GPIO subsystem;
	 * 1 if only IntMask1, IntPending1, IntStatus1, IntAck1 are used;
	 * 2 if IntMask1, IntMask2, IntPending1, IntPending2, ... are used;
	 * ...
	 * Valid irq_reg_idx values belong to range: [0; num_irq_registers - 1].
	 * */
	unsigned (*get_num_irq_regs)(void);
	/* The following function returns a mask, which defines bits in FPGA
	 * interrupt registers (IntMask, IntPending, IntStatus, IntAck).
	 * If some bit in this mask is set to 1, then corresponding interrupt
	 * should be routed (by our driver) to GPIO subsystem. */
	u16 (*get_irq_mask)(unsigned irq_reg_idx);
	/* Get GPIO line offset by IRQ register index and bit in IRQ register */
	unsigned (*get_offset)(unsigned irq_reg_idx, unsigned bit);
	/* Get IRQ register index by GPIO line offset */
	unsigned (*get_irq_reg_idx)(unsigned offset);
	/* Get bit in IRQ register by GPIO line offset */
	unsigned (*get_irq_reg_bit)(unsigned offset);
	/* Get IRQ register address by index */
	u16 (*get_IntMask_reg)(unsigned irq_reg_idx);
	u16 (*get_IntPending_reg)(unsigned irq_reg_idx);
	u16 (*get_IntStatus_reg)(unsigned irq_reg_idx);
	u16 (*get_IntAck_reg)(unsigned irq_reg_idx);
};

extern struct nm_fpga_gpio_config nm_fpga_gpio_08;
extern struct nm_fpga_gpio_config nm_fpga_gpio_12;

extern struct nm_fpga_gpio_config nm_fpga_gpio_14;
extern struct nm_fpga_gpio_config nm_fpga_gpio_17;
extern struct nm_fpga_gpio_config nm_fpga_gpio_18;

#endif /* __MFD_NM_FPGA_GPIO_H */
