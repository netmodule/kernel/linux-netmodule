/* da9063-comparator.c - Comparator device driver for DA9063
 * Copyright (C) 2018  NetModule
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/delay.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/regmap.h>
#include <linux/slab.h>
#include <linux/workqueue.h>
#include <linux/gpio.h>

#include <linux/mfd/da9063/registers.h>
#include <linux/mfd/da9063/core.h>

#define DA9063_COMPARATOR_WORK_DELAY_MS	1000

typedef enum {
	DA9063_COMPARATOR_STATUS_UNKNOWN = -1,
	DA9063_COMPARATOR_STATUS_LOW,
	DA9063_COMPARATOR_STATUS_HIGH
} DA9063_COMPARATOR_STATUS;

struct da9063_comparator_regmap {
	/* REGS */
	int comp_enable_reg;
	int comp_status_reg;
	int comp_event_reg;
	int comp_irq_disable_reg;
	/* MASKS */
	int comp_enable_mask;
	int comp_status_mask;
	int comp_event_mask;
	int comp_irq_disable_mask;
	/* NAMES */
	const char *name;
};

struct da9063_comparator {
	struct regmap *regmap;
	const struct da9063_comparator_regmap *config;
	DA9063_COMPARATOR_STATUS output;
	struct delayed_work work;
	struct gpio_chip gp;
};

static const struct da9063_comparator_regmap da9063_regmap = {
	/* REGS */
	.comp_enable_reg            = DA9063_REG_ADC_CONT,
	.comp_status_reg            = DA9063_REG_STATUS_A,
	.comp_event_reg             = DA9063_REG_EVENT_B,
	.comp_irq_disable_reg       = DA9063_REG_IRQ_MASK_B,
	/* MASKS */
	.comp_enable_mask           = DA9063_COMP1V2_EN,
	.comp_status_mask           = DA9063_COMP_1V2,
	.comp_event_mask            = DA9063_E_COMP_1V2,
	.comp_irq_disable_mask      = DA9063_M_COMP_1V2,
	/* NAMES */
	.name = "da9062-comparator",
};

static int da9063_comparator_status_update(struct da9063_comparator *comparator)
{
	const struct da9063_comparator_regmap *config = comparator->config;
	unsigned int val;
	int ret;

	ret = regmap_read(comparator->regmap,
			  config->comp_status_reg,
			  &val);
	if (!ret) {
		comparator->output = (val & config->comp_status_mask) ?
				DA9063_COMPARATOR_STATUS_HIGH : DA9063_COMPARATOR_STATUS_LOW;
		if (comparator->output == DA9063_COMPARATOR_STATUS_LOW) {
			ret = 0;
		}
		else if (comparator->output == DA9063_COMPARATOR_STATUS_HIGH) {
			ret = 1;
		}
	}
	return ret;
}

static int da9063_gpio_get(struct gpio_chip *gc, unsigned offset)
{
	struct da9063_comparator *comparator;
	comparator = container_of(gc, struct da9063_comparator, gp);
	return da9063_comparator_status_update(comparator); 
}

static int da9063_get_direction(struct gpio_chip *gc, unsigned int offset)
{
	return 1;
}

static const struct of_device_id da9063_comparator_of_match_table[] = {
	{ .compatible = "dlg,da9063-comparator", .data = &da9063_regmap },
	{ },
};
MODULE_DEVICE_TABLE(of, da9063_comparator_of_device_id);

static int da9063_comparator_probe(struct platform_device *pdev)
{
	struct da9063 *da9063 = dev_get_drvdata(pdev->dev.parent);
	struct da9063_comparator *comparator;
	const struct of_device_id *match;
	int ret;

	if (!da9063 || !da9063->dev->parent->of_node) {
		return -EPROBE_DEFER;
	}

	if (!pdev->dev.of_node)
		return -ENXIO;

	comparator = devm_kzalloc(&pdev->dev, sizeof(*comparator), GFP_KERNEL);
	if (!comparator)
		return -ENOMEM;
	
	comparator->gp.label = "da9063-comparator";
	comparator->gp.owner = THIS_MODULE;
	comparator->gp.get = da9063_gpio_get;
	comparator->gp.get_direction = da9063_get_direction;
	comparator->gp.can_sleep = 1;
	comparator->gp.ngpio = 1;
	comparator->gp.base = -1;
	comparator->gp.of_node = pdev->dev.of_node;

	ret =  devm_gpiochip_add_data(&pdev->dev, &comparator->gp, comparator);
	if (ret < 0) {
		dev_err(&pdev->dev, "Could not register gpiochip, %d\n", ret);
		return ret;
	}

	platform_set_drvdata(pdev, comparator);

	comparator->regmap = dev_get_regmap(pdev->dev.parent, NULL);
	if (!comparator->regmap) {
		dev_warn(&pdev->dev, "Parent regmap unavailable.\n");
		return -ENXIO;
	}

	match = of_match_node(da9063_comparator_of_match_table,
				pdev->dev.of_node);
	if (!comparator->regmap) {
		dev_warn(&pdev->dev, "Can't match compatible device.\n");
		return -ENXIO;
	}

	comparator->config = match->data;
	comparator->output = DA9063_COMPARATOR_STATUS_UNKNOWN;

	platform_set_drvdata(pdev, comparator);

	return 0;
}

static struct platform_driver da9063_comparator_driver = {
	.probe		= da9063_comparator_probe,
	.driver		= {
		.name	= DA9063_DRVNAME_COMPARATOR,
		.of_match_table = da9063_comparator_of_match_table,
	},
};

module_platform_driver(da9063_comparator_driver);

MODULE_AUTHOR("Wieslaw Kubala <wieslaw.kubala@netmodule.com>");
MODULE_DESCRIPTION("DA9063 comparator driver");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:" DA9063_DRVNAME_COMPARATOR);
