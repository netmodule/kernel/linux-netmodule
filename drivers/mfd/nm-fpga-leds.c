/*
 * fpga-leds.c - Provides access to leds connected to FPGA on NRHW18
 *
 * Copyright (C) 2018 NetModule AG
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*-------------------------------------------------------------------------*/

#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/memory.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <linux/seq_file.h>
#include <asm/uaccess.h>
#include <linux/mfd/core.h>
#include <linux/regmap.h>
#include <linux/of_platform.h>
#include <linux/leds.h>

#include <linux/mfd/nm-fpga/core.h>
#include <linux/mfd/nm-fpga/registers.h>

#define LED_OFF 0
#define FPGA_MAX_LEDS 12
#define FPGA_LED_MAX_BRIGHTNESS 255

struct fpga_compatible_leds;

struct fpga_led {
	struct led_classdev	cdev;
	struct fpga_compatible_leds *parent;
	unsigned int id;
};

struct fpga_compatible_leds {
	struct device *dev;
	struct regmap *regmap;
	struct mutex lock;
	struct fpga_led *led[FPGA_MAX_LEDS];
};

/*-------------------------------------------------------------------------*/
static int fpga_led_set_brightness(struct led_classdev *cdev,
                                   enum led_brightness brightness)
{
	struct fpga_led *led = container_of(cdev, struct fpga_led, cdev);
	struct fpga_compatible_leds *leds = led->parent;
	unsigned int mask;
	int ret;

	mutex_lock(&leds->lock);

	mask = 1UL << led->id;
	ret = regmap_update_bits(leds->regmap,
				NM_FPGA_REG_LEDS,
				mask,
				brightness == 0 ? 0x00 : mask);
	if (ret < 0) {
		dev_err(leds->dev, "Failed to set LED %d\n", led->id);
		goto out;
	}

/*	led->current_brightness = brightness;*/

out:
	mutex_unlock(&leds->lock);

	return ret;
}


static int fpga_leds_probe(struct platform_device * pdev)
{
	struct device *dev = &pdev->dev;
	struct device_node *np = pdev->dev.of_node;
	struct fpga_compatible_leds *leds;
	struct device_node *child;
	int ret;
	
	printk(KERN_INFO "fpga_leds_probe\n");
	if (!pdev->dev.of_node)
		return -ENXIO;

	leds = devm_kzalloc(dev, sizeof(*leds), GFP_KERNEL);
	if (!leds)
		return -ENOMEM;

	leds->dev = dev;
	leds->regmap = dev_get_regmap(pdev->dev.parent, NULL);
	if (!leds->regmap) {
		dev_warn(&pdev->dev, "Parent regmap unavailable.\n");
		return -ENXIO;
	}
	platform_set_drvdata(pdev, leds);
	mutex_init(&leds->lock);
	
	for_each_available_child_of_node(np, child) {
		struct fpga_led *led;
		u32 reg;
		
		ret = of_property_read_u32(child, "reg", &reg);
		if (ret) {
			dev_err(dev, "Failed to read led 'reg' property\n");
			goto put_child_node;
		}

		if (reg >= FPGA_MAX_LEDS || leds->led[reg]) {
			dev_err(dev, "Invalid led reg %u\n", reg);
			ret = -EINVAL;
			goto put_child_node;
		}

		led = devm_kzalloc(dev, sizeof(*led), GFP_KERNEL);
		if (!led) {
			ret = -ENOMEM;
			goto put_child_node;
		}
		
		led->id = reg;
		led->parent = leds;
		led->cdev.max_brightness = FPGA_LED_MAX_BRIGHTNESS;
		led->cdev.brightness_set_blocking =	fpga_led_set_brightness;
		led->cdev.name = of_get_property(child, "label", NULL) ? : child->name;
		led->cdev.default_trigger = of_get_property(child,
		                                            "linux,default-trigger",
		                                            NULL);

		leds->led[reg] = led;
/*
		ret = da9063_led_set_dt_default(&led->cdev, child);
		if (ret < 0) {
			dev_err(dev,
				"Failed to LED set default from devicetree\n");
			goto put_child_node;
		}
*/
		
		ret = devm_led_classdev_register(dev, &led->cdev);
		
		if (ret) {
			dev_err(dev, "Failed to register LED: %d\n", ret);
			goto put_child_node;
		}
		
		led->cdev.dev->of_node = child;
		
	}
	printk(KERN_INFO "fpga leds probe successful\n");
	return 0;
	
put_child_node:
	of_node_put(child);
	return ret;


}

static int fpga_leds_remove(struct platform_device * pdev)
{
	struct fpga_compatible_leds *leds = platform_get_drvdata(pdev);
	int i;

	printk(KERN_INFO "fpga_leds_remove\n");
	/* Turn the LEDs off on driver removal. */
	for (i = 0 ; i < FPGA_MAX_LEDS; i++) {
		if (leds->led[i]) {
			fpga_led_set_brightness(&leds->led[i]->cdev, LED_OFF);
		}
	}

	mutex_destroy(&leds->lock);

	return 0;
}
/*-------------------------------------------------------------------------*/
/*
static const struct of_device_id nm_fpga_of_match[] = {
	{ .compatible = "netmodule,fpga-led" },
	{ },
};
MODULE_DEVICE_TABLE(of, nm_fpga_of_match);
*/
static struct platform_driver fpga_leds_driver = {
	.driver = {
		.name		   = NM_FPGA_DRVNAME_LEDS,
		//.of_match_table = nm_fpga_of_match,
	},
	.probe = fpga_leds_probe,
	.remove = fpga_leds_remove,
};

module_platform_driver(fpga_leds_driver);

MODULE_DESCRIPTION("Driver for leds connected to fpga of nmrouter / NRHW18");
MODULE_AUTHOR("Alexandre Bard");
MODULE_AUTHOR("Netmodule AG");
MODULE_LICENSE("GPL");
