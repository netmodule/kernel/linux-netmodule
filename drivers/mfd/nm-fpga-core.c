/*
 * nm-fpga-core.c - NetModule FPGA core driver
 *
 * Copyright (C) 2018 NetModule AG
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/of_platform.h>

#include <linux/regmap.h>
#include <linux/interrupt.h>

#include "nm-fpga.h"

#define NM_FPGA_REG_ID            0x0000
#define NM_FPGA_REG_VERSION       0x0002

static const struct regmap_config nm_fpga_regmap_config = {
	.reg_bits = 32,
	.val_bits = 16,
	.reg_stride = 2,
	.fast_io = true,
#ifdef __BIG_ENDIAN
	.val_format_endian = REGMAP_ENDIAN_BIG,
#else
	.val_format_endian = REGMAP_ENDIAN_LITTLE,
#endif
};

static int nm_fpga_core_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	void __iomem *base;
	struct resource *res;
	struct nm_fpga *fpga;
	unsigned val;
	int ret;

	fpga = devm_kzalloc(dev, sizeof(*fpga), GFP_KERNEL);
	if (!fpga)
		return -ENOMEM;
	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res) {
		dev_err(dev, "cannot get IORESOURCE_MEM\n");
		return -ENXIO;
	}
	base = devm_ioremap_resource(dev, res);
	if (IS_ERR(base)) {
		dev_err(dev, "ioremap failed\n");
		return PTR_ERR(base);
	}
	fpga->regmap = devm_regmap_init_mmio(dev, base, &nm_fpga_regmap_config);
	if (IS_ERR(fpga->regmap)) {
		dev_err(dev, "regmap initialization failed\n");
		return PTR_ERR(fpga->regmap);
	}
	fpga->irq = platform_get_irq(pdev, 0);
	if (fpga->irq < 0) {
		dev_err(dev, "cannot get irq\n");
		return fpga->irq;
	}
	dev_set_drvdata(dev, fpga);

	regmap_read(fpga->regmap, NM_FPGA_REG_ID, &val);
	dev_info(dev, "FPGA ID: 0x%04x\n", val);
	regmap_read(fpga->regmap, NM_FPGA_REG_VERSION, &val);
	dev_info(dev, "FPGA firmware version: %u.%u\n", (val >> 8),
		 (val & 0xFF));
	ret = devm_of_platform_populate(dev);
	if (ret)
		dev_err(dev, "cannot register subdevices\n");
	else
		dev_info(dev, "%s completed successfully\n", __FUNCTION__);
	return ret;
}

static const struct of_device_id nm_fpga_core_of_match[] = {
	{ .compatible = "netmodule,fpga-core" },
	{ },
};
MODULE_DEVICE_TABLE(of, nm_fpga_core_of_match);

static struct platform_driver nm_fpga_core_driver = {
	.probe = nm_fpga_core_probe,
	.driver = {
		.name = "nm-fpga-core",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(nm_fpga_core_of_match),
	},
};
module_platform_driver(nm_fpga_core_driver);

MODULE_DESCRIPTION("NetModule FPGA core driver");
MODULE_AUTHOR("Alexandre Bard");
MODULE_AUTHOR("Viacheslav Volkov");
MODULE_AUTHOR("NetModule AG");
MODULE_LICENSE("GPL");
