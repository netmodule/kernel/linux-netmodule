/*
 * fpga-sim.c - Allows control of the sim selector through the FPGA
 *
 * Copyright (C) 2018 NetModule AG
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*-------------------------------------------------------------------------*/

#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/memory.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <linux/seq_file.h>
#include <asm/uaccess.h>
#include <linux/mfd/core.h>
#include <linux/regmap.h>
#include <linux/of_platform.h>
#include<linux/sysfs.h> 
#include<linux/kobject.h> 

#include <linux/mfd/nm-fpga/core.h>
#include <linux/mfd/nm-fpga/registers.h>

#define SIM_MASK 0x7
#define SIM_1_NAME sim1
#define SIM_2_NAME sim2


/*
typedef enum {
	DISCONNECT,
	PCIE_1,
	PCIE_2,
	EXT_SLOT,
	LOW_END
};
*/

struct fpga_sim_desc {
	unsigned int reg;
	unsigned int offset;
};

static struct fpga_sim_desc sim_desc[] = {
	{NM_FPGA_REG_SIM_CTRL, 0}, // SIM1
	{NM_FPGA_REG_SIM_CTRL, 4}, // SIM2
};

struct fpga_sim {
	struct device *dev;
	struct regmap *regmap;
	struct mutex lock;
	struct kobject kobj;
};


static int fpga_sim_get(struct fpga_sim * sim, unsigned int sim_nbr)
{
	struct regmap * regmap = sim->regmap;
	unsigned val;
	if(sim_nbr >= ARRAY_SIZE(sim_desc))
		return -1;
	regmap_read(regmap, sim_desc[sim_nbr].reg, &val);
	val = (val >> sim_desc[sim_nbr].offset) & SIM_MASK;
	return val;
}

static void fpga_sim_set(struct fpga_sim * sim, unsigned int sim_nbr,
                         int value)
{
	struct regmap * regmap = sim->regmap;
	unsigned mask;
	
	if(sim_nbr >= ARRAY_SIZE(sim_desc)){
		printk(KERN_ERR "sim_set : sim_nbr out of range\n");
		return;
	}
	
	mask = SIM_MASK << sim_desc[sim_nbr].offset;
	value <<= sim_desc[sim_nbr].offset;
	regmap_update_bits(regmap,
	                   sim_desc[sim_nbr].reg,
	                   mask,
	                   value);
}

/*************** Sysfs Functions **********************/
static ssize_t sysfs_show(struct kobject * kobj, struct kobj_attribute *attr, char *buf){
	struct fpga_sim * sim = container_of(kobj, struct fpga_sim, kobj);
	int value;
	int sim_nbr = strcmp(attr->attr.name, __stringify(SIM_1_NAME)) == 0 ? 0 : 1;
	
	mutex_lock(&sim->lock);
	value = fpga_sim_get(sim, sim_nbr);
	mutex_unlock(&sim->lock);
	
	sprintf(buf, "%x", value);
	return strlen(buf);
}
static ssize_t sysfs_store(struct kobject * kobj, struct kobj_attribute *attr, const char *buf, size_t count){
	struct fpga_sim * sim = container_of(kobj, struct fpga_sim, kobj);
	long value;
	int ret;
	int sim_nbr = strcmp(attr->attr.name, __stringify(SIM_1_NAME)) == 0 ? 0 : 1;
	ret = kstrtol(buf, 16, &value);
	if(0 != ret){
		printk(KERN_ERR "Invalid value for SIM switch : %s, error : %d\n", buf, ret);
		return 0;
	}
	mutex_lock(&sim->lock);
	fpga_sim_set(sim, sim_nbr, value);
	mutex_unlock(&sim->lock);
	return count;
}


static struct kobj_attribute sim1_attr = __ATTR(SIM_1_NAME, 0660, sysfs_show, sysfs_store);
static struct kobj_attribute sim2_attr = __ATTR(SIM_2_NAME, 0660, sysfs_show, sysfs_store);
static struct attribute *attrs[] = {
	&sim1_attr.attr,
	&sim2_attr.attr,
	NULL,
};

static ssize_t nm_fpga_sim_sysfs_show(struct kobject *kobj, struct attribute * attr, char * buf){
	struct kobj_attribute * kattr = container_of(attr, struct kobj_attribute, attr);
	return kattr->show(kobj, kattr, buf);
}

static ssize_t nm_fpga_sim_sysfs_store(struct kobject *kobj, struct attribute * attr, const char * buf, size_t count){
	struct kobj_attribute * kattr = container_of(attr, struct kobj_attribute, attr);
	return kattr->store(kobj, kattr, buf, count);
}

static const struct sysfs_ops sim_sysfs_ops = {
	.show = nm_fpga_sim_sysfs_show,
	.store =  nm_fpga_sim_sysfs_store,
};

static struct kobj_type sim_type = {
	.sysfs_ops = &sim_sysfs_ops,
	.default_attrs = attrs, //struct attribute **
};

static int fpga_sim_probe(struct platform_device * pdev)
{
	struct device *dev = &pdev->dev;
	struct fpga_sim * sim;
	int ret;
	
	printk(KERN_INFO "fpga_sim_probe\n");
	
	sim = devm_kzalloc(dev, sizeof(*sim), GFP_KERNEL);
	if(!sim)
		return -ENOMEM;
	
	sim->regmap = dev_get_regmap(pdev->dev.parent, NULL);
	
	
	if (!sim->regmap) {
		dev_warn(&pdev->dev, "Parent regmap unavailable.\n");
		return -ENXIO;
	}
	
	sim->dev = dev;
	ret = kobject_init_and_add(&sim->kobj, &sim_type, NULL, "sim-ctrl");
	
	if(ret < 0){
		dev_err(dev, "Failed to create SIM kobject : %d\n", ret);
		return ret;
	}
	
	mutex_init(&sim->lock);
	platform_set_drvdata(pdev, sim);
	
	return 0;
}

static int fpga_sim_remove(struct platform_device * pdev)
{
	struct device *dev = &pdev->dev;
	struct fpga_sim * sim = platform_get_drvdata(pdev);
	
	mutex_destroy(&sim->lock);
	devm_kfree(dev, sim);
	return 0;
}
/*-------------------------------------------------------------------------*/

static struct platform_driver fpga_sims_driver = {
	.driver = {
		.name		   = NM_FPGA_DRVNAME_SIMS,
	},
	.probe = fpga_sim_probe,
	.remove = fpga_sim_remove,
};

module_platform_driver(fpga_sims_driver);

MODULE_DESCRIPTION("Driver for SIM control through fpga of nmrouter / NRHW18");
MODULE_AUTHOR("Alexandre Bard");
MODULE_AUTHOR("Netmodule AG");
MODULE_LICENSE("GPL");
