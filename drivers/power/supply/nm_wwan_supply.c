/*
 * nm_wwan_supply.c
 *
 * Copyright 2020 NetModule AG
 *
 * Author: Alexandre Bard <alexandre.bard@netmodule.com>
 *
 * Based of userspace consumer driver:
 *   Copyright 2009 CompuLab, Ltd.
 *   Author: Mike Rapoport <mike@compulab.co.il>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 */

#include <linux/err.h>
#include <linux/mutex.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/regulator/consumer.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/delay.h>

#define DEFAULT_MIN_DOWNTIME_US 500000

struct wwan_supply_data {
	const char *name;
	struct mutex lock;
	bool enabled;
	struct regulator *supply;
};

static ssize_t reg_show_name(struct device *dev,
			  struct device_attribute *attr, char *buf)
{
	struct wwan_supply_data *pdata = dev_get_drvdata(dev);

	return snprintf(buf, PAGE_SIZE, "%s\n", pdata->name);
}

static ssize_t reg_show_state(struct device *dev,
			  struct device_attribute *attr, char *buf)
{
	struct wwan_supply_data *pdata = dev_get_drvdata(dev);

	if (pdata->enabled)
		return snprintf(buf, PAGE_SIZE, "enabled\n");

	return snprintf(buf, PAGE_SIZE, "disabled\n");
}

static ssize_t reg_set_state(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	struct wwan_supply_data *pdata = dev_get_drvdata(dev);
	bool enabled;
	int ret;

	if (sysfs_streq(buf, "enabled") || sysfs_streq(buf, "1")) {
		enabled = true;
	}
	else if (sysfs_streq(buf, "disabled") || sysfs_streq(buf, "0")) {
		enabled = false;
	}
	else {
		dev_err(dev, "Configuring invalid mode\n");
		return -EINVAL;
	}

	mutex_lock(&pdata->lock);
	if (enabled != pdata->enabled) {
		if (enabled)
			ret = regulator_enable(pdata->supply);
		else
			ret = regulator_disable(pdata->supply);

		if (ret == 0)
			pdata->enabled = enabled;
		else
			dev_err(dev, "Failed to configure state: %d\n", ret);
	}
	mutex_unlock(&pdata->lock);

	return count;
}

static DEVICE_ATTR(name, 0444, reg_show_name, NULL);
static DEVICE_ATTR(state, 0644, reg_show_state, reg_set_state);

static struct attribute *attributes[] = {
	&dev_attr_name.attr,
	&dev_attr_state.attr,
	NULL,
};

static const struct attribute_group attr_group = {
	.attrs	= attributes,
};

static int nm_wwan_supply_probe(struct platform_device *pdev)
{
	struct wwan_supply_data *pdata;
	struct device_node *np = pdev->dev.of_node;
	int ret;
	u32 min_downtime_us;

	pdata = devm_kzalloc(&pdev->dev, sizeof(*pdata), GFP_KERNEL);
	if (!pdata)
		return -ENOMEM;


	mutex_init(&pdata->lock);

	pdata->name = of_get_property(np, "name", NULL);
	pdata->supply = devm_regulator_get(&pdev->dev, "wwan");
	if (IS_ERR(pdata->supply)) {
		if (PTR_ERR(pdata->supply) != -EPROBE_DEFER)
			dev_err(&pdev->dev, "Failed to get supply: %ld\n", PTR_ERR(pdata->supply));
		return PTR_ERR(pdata->supply);
	}


	// Initial startup sequence: keep the line down for "min-downtime"
	if (of_property_read_u32(np, "min-downtime-us", &min_downtime_us)) {
		dev_info(&pdev->dev,
		    "Using default downtime before powering modem");
		min_downtime_us = DEFAULT_MIN_DOWNTIME_US;
	}

	dev_dbg(&pdev->dev, "Keeping modem down for %d us", min_downtime_us);
	usleep_range(min_downtime_us, min_downtime_us + min_downtime_us / 10);

	ret = regulator_enable(pdata->supply);
	if (ret) {
		dev_err(&pdev->dev,
			"Failed to bring up modem power line: %d\n", ret);
		return ret;
	}

	pdata->enabled = true;

	// Expose sysfs entry
	ret = sysfs_create_group(&pdev->dev.kobj, &attr_group);
	if (ret != 0)
		return ret;

	platform_set_drvdata(pdev, pdata);

	return 0;
}

static int nm_wwan_supply_remove(struct platform_device *pdev)
{
	sysfs_remove_group(&pdev->dev.kobj, &attr_group);
	return 0;
}

static void nm_wwan_supply_shutdown(struct platform_device *pdev)
{
	struct wwan_supply_data *pdata = platform_get_drvdata(pdev);
	if (pdata->enabled)
		regulator_disable(pdata->supply);
}

#if defined(CONFIG_OF)
static const struct of_device_id wwan_of_match[] = {
	{ .compatible = "nm,wwan-supply", },
	{},
};
MODULE_DEVICE_TABLE(of, wwan_of_match);
#endif

static struct platform_driver nm_wwan_supply_driver = {
	.probe		= nm_wwan_supply_probe,
	.remove		= nm_wwan_supply_remove,
	.shutdown	= nm_wwan_supply_shutdown,
	.driver		= {
		.name		= "wwan-supply",
		.of_match_table = of_match_ptr(wwan_of_match),
	},
};

module_platform_driver(nm_wwan_supply_driver);

MODULE_AUTHOR("Alexandre Bard <alexandre.bard@netmodule.com>");
MODULE_DESCRIPTION("WWAN supply controller for NetModule systems");
MODULE_LICENSE("GPL");
