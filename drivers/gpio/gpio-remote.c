/*
 *  Remote GPIO driver for ZF Aurix user module
 *
 *  Copyright (C) 2018 NetModule AG
 *
 *  Author: Andrejs Cainikovs <andrejs.cainikovs@netmodule.com>
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 *
 */

#include <linux/device.h>
#include <linux/gpio/driver.h>
#include <linux/inet.h>
#include <linux/kthread.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <net/sock.h>
#include <net/tcp.h>

MODULE_AUTHOR("Andrejs Cainikovs");
MODULE_DESCRIPTION("Remote GPIO driver");
MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");

/* Defines / config */

#define DEVICE_NAME     "remote-gpio"

#define SERVER_IP       (192 << 24) | (168 << 16) | (1 << 8) | (42)
#define SERVER_PORT     6666

#define CMD_INPUT       'I'
#define CMD_OUTPUT      'O'

#define MESSAGE_SIZE    4 /* IXXY/OXXY */
#define RECONNECT_DELAY 1000
#define MAX_GPIOS        32

/* Semaphore helper macros */

#define DOWN(x) do { \
                       if (!down_interruptible(x)) \
					   { \
					       break; \
					   } \
                   } while (1)

#define UP(x) up(x)

/* Default GPIO names */

static const char * default_gpio_names[MAX_GPIOS] =
{
	"RGPIO0",  "RGPIO1",  "RGPIO2",  "RGPIO3",
	"RGPIO4",  "RGPIO5",  "RGPIO6",  "RGPIO7",
	"RGPIO8",  "RGPIO9",  "RGPIO10", "RGPIO11",
	"RGPIO12", "RGPIO13", "RGPIO14", "RGPIO15",
	"RGPIO16", "RGPIO17", "RGPIO18", "RGPIO19",
	"RGPIO20", "RGPIO21", "RGPIO22", "RGPIO23",
	"RGPIO24", "RGPIO25", "RGPIO26", "RGPIO27",
	"RGPIO28", "RGPIO29", "RGPIO30", "RGPIO31"
};

/* Driver data struct */

struct remote_gpio_data
{
	struct device      *device;
	struct gpio_chip    gpio;
	struct task_struct *thread;
	struct socket      *socket;
	struct sockaddr_in  sin;
	struct semaphore    semaphore;
	int                 connected;
	unsigned int        ip;
	unsigned short      port;

	DECLARE_BITMAP(direction, MAX_GPIOS);
	DECLARE_BITMAP(inputs,    MAX_GPIOS);
	DECLARE_BITMAP(outputs,   MAX_GPIOS);
};

/* Driver data / variables */

static struct remote_gpio_data *data;
static struct class *class;
static dev_t dev_num;

/* Forward declarations */

static int htoi(char c);
static int set_ip(const char *str);

/* SysFS functions */

static ssize_t sysfs_ip_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	uint32_t x;
	unsigned char ip[4];

	x = data->ip;

	ip[0] = x >> 24;
	ip[1] = (x >> 16) & 0xFF;
	ip[2] = (x >>  8) & 0xFF;
	ip[3] = x & 0xFF;

	return sprintf(buf, "%u.%u.%u.%u\n", ip[0], ip[1], ip[2], ip[3]);
}

static ssize_t sysfs_ip_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	int rc;

	/* Set new IP */

	rc = set_ip(buf);
	if (rc < 0)
	{
		return rc;
	}

	/* Disconnect */

	DOWN(&data->semaphore);
	if (data->connected)
	{
		kernel_sock_shutdown(data->socket, SHUT_RDWR);
		sock_release(data->socket);
		data->connected = 0;
	}
	UP(&data->semaphore);

	return count;
}

static ssize_t sysfs_port_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%u\n", data->port);
}

static ssize_t sysfs_port_store(struct device *dev, struct device_attribute *attr, const  char *buf, size_t count)
{
	int rc;

	/* Set new port */

	rc = kstrtou16(buf, 10, &data->port);
	if (rc < 0)
	{
		return rc;
	}

	/* Disconnect */

	DOWN(&data->semaphore);
	if (data->connected)
	{
		kernel_sock_shutdown(data->socket, SHUT_RDWR);
		sock_release(data->socket);
		data->connected = 0;
	}
	UP(&data->semaphore);

	return count;
}

/* SysFS entries */

static DEVICE_ATTR(ip,   S_IRUGO | S_IWUSR, sysfs_ip_show,   sysfs_ip_store);
static DEVICE_ATTR(port, S_IRUGO | S_IWUSR, sysfs_port_show, sysfs_port_store);

static struct attribute *rgpio_sysfs_attrs[] =
{
	&dev_attr_ip.attr,
	&dev_attr_port.attr,
	NULL
};

static const struct attribute_group rgpio_sysfs_group =
{
	.name = "config",
	.attrs = rgpio_sysfs_attrs
};

/* Driver code */

static int htoi(char c)
{
	int ret;

	if (c >= '0' && c <= '9')
	{
		ret = c - '0';
	}
	else
	if (c >= 'a' && c <= 'f')
	{
		ret = c - 'a' + 10;
	}
	else
	if (c >= 'A' && c <= 'F')
	{
		ret = c - 'A' + 10;
	}
	else
	{
		ret = -EINVAL;
	}

	return ret;
}

static int set_ip(const char *str)
{
	int rc, i, ip[4];

	/* Validate IP */

	rc = sscanf(str, "%d.%d.%d.%d", &ip[0], &ip[1], &ip[2], &ip[3]);
	if (rc != 4)
	{
		return -EINVAL;
	}
	for (i = 0; i < 4; i++)
	{
		if (ip[i] < 0 || ip[i] > 255)
		{
			return -EINVAL;
		}
	}

	/* Set new IP */

	data->ip = ntohl(in_aton(str));

	return 0;
}

static int receive_input(void)
{
	struct msghdr msg;
	struct kvec iov;
	char *buffer;
	unsigned int pin, tmp;
	int value;
	int rc = 0;

	/* Allocate message buffer */

	buffer = kmalloc(MESSAGE_SIZE, GFP_KERNEL);
	if (!buffer)
	{
		printk(KERN_ERR "%s: could not allocate message buffer\n", DEVICE_NAME);

		return -ENOMEM;
	}

	/* Prepare message */

	iov.iov_base = buffer;
	iov.iov_len = MESSAGE_SIZE;

	msg.msg_control = NULL;
	msg.msg_controllen = 0;
	msg.msg_flags = MSG_DONTWAIT;
	msg.msg_name = &data->sin;
	msg.msg_namelen = sizeof(struct sockaddr_in);

	/* Try to receive a message */

	DOWN(&data->semaphore);
	if (data->connected)
	{
		rc = kernel_recvmsg(data->socket, &msg, &iov, 1, MESSAGE_SIZE, msg.msg_flags);
		if (rc <= 0)
		{
			if (rc == 0)
			{
				printk(KERN_INFO "%s: socket disconnected\n", DEVICE_NAME);

				kernel_sock_shutdown(data->socket, SHUT_RDWR);
				sock_release(data->socket);
				data->connected = 0;
			}
			else
			if (rc != -EAGAIN)
			{
				printk(KERN_WARNING "%s: could not receive a message: %d\n", DEVICE_NAME, rc);
			}
			kfree(buffer);
			UP(&data->semaphore);
			return -ENOMEM;
		}
	}
	UP(&data->semaphore);

	/* Parse message */

	do
	{
		if (rc == MESSAGE_SIZE && buffer[0] == CMD_INPUT)
		{
			/* Extract pin number */

			tmp = htoi(buffer[1]);
			if (tmp < 0)
			{
				break;
			}
			pin = tmp << 4;

			tmp = htoi(buffer[2]);
			if (tmp < 0)
			{
				break;
			}
			pin += tmp;

			/* Extract pin value */

			if (buffer[3] == '0' || buffer[3] == '1')
			{
				value = buffer[3] - '0';
			}
			else
			{
				break;
			}

			/* Update input */

			value ? set_bit(pin, data->inputs) : clear_bit(pin, data->inputs);

			printk(KERN_DEBUG "%s: input pin %u set to %u\n", DEVICE_NAME, pin, value);
		}
		else
		{
			break;
		}
	}
	while (0);

	/* Free message buffer */

	kfree(buffer);

	/* Done */

	return rc;
}

static int send_output(unsigned int pin, int value)
{
	struct msghdr msg;
	struct kvec iov;
	char *buffer;
	int len;
	int rc;

	/* Allocate message buffer */

	buffer = kmalloc(MESSAGE_SIZE + 1, GFP_KERNEL);
	if (!buffer)
	{
		printk(KERN_ERR "%s: could not allocate message buffer\n", DEVICE_NAME);

		return -ENOMEM;
	}

	/* Assemble message */

	rc = snprintf(buffer, MESSAGE_SIZE + 1, "%c%02X%u", CMD_OUTPUT, pin, value ? 1 : 0);
	if (rc < 0 || rc >= MESSAGE_SIZE + 1)
	{
		printk(KERN_ERR "%s: could not assemble message\n", DEVICE_NAME);

		kfree(buffer);
		return (rc < 0) ? -ENOMEM : -EINVAL;
	}
	len = rc;

	/* Prepare message */

	iov.iov_base = buffer;
	iov.iov_len = len;

	msg.msg_control = NULL;
	msg.msg_controllen = 0;
	msg.msg_flags = 0;
	msg.msg_name = 0;
	msg.msg_namelen = 0;

	/* Send message */

	DOWN(&data->semaphore);
	if (data->connected)
	{
		rc = kernel_sendmsg(data->socket, &msg, &iov, 1, len);
		if (rc < 0)
		{
			printk(KERN_WARNING "%s: could not send a message: %d\n", DEVICE_NAME, rc);
		}
	}
	UP(&data->semaphore);

	/* Free message buffer */

	kfree(buffer);

	/* Done */

	printk(KERN_DEBUG "%s: output pin %u set to %u\n", DEVICE_NAME, pin, value);

	return rc;
}

static int thread(struct remote_gpio_data *data)
{
	int rc;

	printk(KERN_INFO "%s: thread started\n", DEVICE_NAME);

	/* Thread loop */

	while (!kthread_should_stop())
	{
		/* Connect/reconnect */

		DOWN(&data->semaphore);
		if (!data->connected)
		{
			/* Set up socket destination */

			data->sin.sin_addr.s_addr = htonl(data->ip);
			data->sin.sin_family = AF_INET;
			data->sin.sin_port = htons(data->port);

			/* Create socket */

			rc = sock_create_kern(&init_net, PF_INET, SOCK_STREAM, IPPROTO_TCP, &data->socket);
			if (rc < 0)
			{
				printk(KERN_ERR "%s: could not create a socket\n", DEVICE_NAME);

				UP(&data->semaphore);

				return -1;
			}

			/* Disable Nagles algorithm */

			tcp_sock_set_nodelay(data->socket->sk);

			/* Connect to server */

			rc = kernel_connect(data->socket, (struct sockaddr*)&data->sin, sizeof(struct sockaddr_in), 0);
			if (rc < 0)
			{
				sock_release(data->socket);

				UP(&data->semaphore);

				/* Sleep */

				set_current_state(TASK_UNINTERRUPTIBLE);
				schedule_timeout(msecs_to_jiffies(RECONNECT_DELAY));

				continue;
			}

			/* Connected */

			data->connected = 1;

			printk(KERN_INFO "%s: socket connected\n", DEVICE_NAME);
		}
		UP(&data->semaphore);

		/* Receive data */

		do
		{
			rc = receive_input();

		} while (rc > 0);

		/* Sleep */

		set_current_state(TASK_INTERRUPTIBLE);
		schedule_timeout(HZ);
	}

	/* Close/release socket */

	DOWN(&data->semaphore);
	if (data->connected)
	{
		printk(KERN_INFO "%s: socket disconnected\n", DEVICE_NAME);

		kernel_sock_shutdown(data->socket, SHUT_RDWR);
		sock_release(data->socket);

		data->connected = 0;
	}
	UP(&data->semaphore);

	/* Done */

	printk(KERN_INFO "%s: thread stopped\n", DEVICE_NAME);

	return 0;
}

static int remote_gpio_get_direction(struct gpio_chip *chip, unsigned int offset)
{
	return test_bit(offset, data->direction);
}

static int remote_gpio_direction_input(struct gpio_chip *chip, unsigned int offset)
{
	set_bit(offset, data->direction);
	return 0;
}

static int remote_gpio_direction_output(struct gpio_chip *chip, unsigned int offset, int value)
{
	clear_bit(offset, data->direction);
	value ? set_bit(offset, data->outputs) : clear_bit(offset, data->outputs);
	send_output(offset, value);
	return 0;
}

static int remote_gpio_get(struct gpio_chip *chip, unsigned int offset)
{
	return test_bit(offset, data->inputs);
}

static void remote_gpio_set(struct gpio_chip *chip, unsigned int offset, int value)
{
	value ? set_bit(offset, data->outputs) : clear_bit(offset, data->outputs);
	send_output(offset, value);
}

/* Module probe/remove functions */

static const struct of_device_id remote_gpio_dt_ids[] = {
	{ .compatible = "remote-gpios", },
	{ }
};
MODULE_DEVICE_TABLE(of, remote_gpio_dt_ids);

static int remote_gpio_probe(struct platform_device *pdev)
{
	struct device *device;
	struct device_node *node;
	int    rc;
	u16    port;
	int    gpios;
	const char *gpio_dt_names[MAX_GPIOS];
	const char *str;
	const char *msg;

	/* Create device */

	rc = alloc_chrdev_region(&dev_num, 0, 1, DEVICE_NAME);
	if (rc < 0)
	{
		msg = "could not register char device number";
		goto error_1;
	}

	class = class_create(THIS_MODULE, DEVICE_NAME);
	if (IS_ERR(class))
	{
		rc = PTR_ERR(class);
		msg = "could not create a class";
		goto error_2;
	}

	device = device_create(class, NULL, dev_num, NULL, "%s", DEVICE_NAME);
	if (IS_ERR(device))
	{
		rc = PTR_ERR(device);
		msg = "could not create a device";
		goto error_3;
	}

	/* Create SysFS entries */

	rc = sysfs_create_group(&device->kobj, &rgpio_sysfs_group);
	if (rc)
	{
		msg = "could not create sysfs group";
		goto error_4;
	}

	/* Allocate driver data */

	data = devm_kzalloc(device, sizeof(*data), GFP_KERNEL);
	if (!data)
	{
		rc = -ENOMEM;
		msg = "could not allocate driver data";
		goto error_5;
	}

	/* Initialize driver data */

	data->device    = device;
	data->ip        = SERVER_IP;
	data->port      = SERVER_PORT;
	data->connected = 0;
	sema_init(&data->semaphore, 1);

	/* Extract IP from device tree */

	rc = of_property_read_string(pdev->dev.of_node, "ip", &str);
	if (!rc)
	{
		rc = set_ip(str);
		if (rc < 0)
		{
			printk(KERN_ERR "%s: Device tree error: bad IP address\n", DEVICE_NAME);
		}
	}

	/* Extract port from device tree */

	rc = of_property_read_u16(pdev->dev.of_node, "port", &port);
	if (!rc)
	{
		data->port = port;
	}

	/* Extract GPIO's from device tree */

	gpios = 0;
	for_each_child_of_node(pdev->dev.of_node, node)
	{
		const char *label;

		if (of_node_cmp(node->name, "remote-gpio") != 0)
		{
			continue;
		}

		if (gpios >= MAX_GPIOS)
		{
			printk(KERN_ERR "%s: Driver supports max %d GPIO's, but device tree contains more GPIO definitions.\n", DEVICE_NAME, MAX_GPIOS);
			break;
		}

		rc = of_property_read_string(node, "label", &label);
		if (rc < 0)
		{
			printk(KERN_ERR "%s: Device tree error: no <label> for GPIO#%d\n", DEVICE_NAME, gpios);
		}

		gpio_dt_names[gpios++] = label;
	}

	/* Initialize bitmaps */

	bitmap_fill(data->direction, MAX_GPIOS); /* Inputs by default */
	bitmap_zero(data->inputs,    MAX_GPIOS);
	bitmap_zero(data->outputs,   MAX_GPIOS);

	/* Register GPIO chip */

	data->gpio.owner = THIS_MODULE;
	data->gpio.label = "remote";
	data->gpio.parent = device;

	data->gpio.base = -1;
	data->gpio.ngpio = gpios ?: MAX_GPIOS;
	data->gpio.names = gpios ? (const char **)gpio_dt_names : default_gpio_names;
	data->gpio.can_sleep = false;

	data->gpio.get_direction = remote_gpio_get_direction;
	data->gpio.direction_input = remote_gpio_direction_input;
	data->gpio.direction_output = remote_gpio_direction_output;
	data->gpio.get = remote_gpio_get;
	data->gpio.set = remote_gpio_set;

	rc = devm_gpiochip_add_data(device, &data->gpio, data);
	if (rc < 0)
	{
		msg = "failed to add gpio chip";
		goto error_5;
	}

	/* Create worker thread */

	data->thread = kthread_run((void *)thread, data, "remote-gpio-thread");
	if (IS_ERR(data->thread))
	{
		rc = PTR_ERR(data->thread);
		msg = "could not create thread";
		goto error_5;
	}

	/* Done */

	printk(KERN_INFO "%s: driver loaded\n", DEVICE_NAME);

	return 0;

	/* Error handling */


error_5:

	sysfs_remove_group(&data->device->kobj, &rgpio_sysfs_group);

error_4:

	device_destroy(class, dev_num);

error_3:

	class_destroy(class);

error_2:

	unregister_chrdev_region(dev_num, 1);

error_1:

	printk(KERN_ERR "%s: %s\n", DEVICE_NAME, msg);

	return rc;
}

static int remote_gpio_remove(struct platform_device *pdev)
{
	/* Stop worker thread */

	kthread_stop(data->thread);

	/* Remove SysFS entries */

	sysfs_remove_group(&data->device->kobj, &rgpio_sysfs_group);

	/* Destroy device and it's class */

	device_destroy(class, dev_num);
	class_destroy(class);
	unregister_chrdev_region(dev_num, 1);

	/* Done */

	printk(KERN_INFO "%s: driver unloaded\n", DEVICE_NAME);

	return 0;
}

static struct platform_driver remote_gpio_platform = {
	.driver = {
		.name = "remote-gpios",
		.of_match_table	= of_match_ptr(remote_gpio_dt_ids),
	},
	.probe	= remote_gpio_probe,
	.remove	= remote_gpio_remove,
};

/* Module init/exit functions */

static int __init remote_gpio_init(void)
{
	return platform_driver_register(&remote_gpio_platform);
}

static void __exit remote_gpio_exit(void)
{
	platform_driver_unregister(&remote_gpio_platform);
}

module_init(remote_gpio_init);
module_exit(remote_gpio_exit);
