/*
 * nbhw_fpga.c - Generic functions for FPGA on NBHW based Netbox designs
 *
 * Copyright (C) 2011-2015 NetModule AG
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include "nbhw_fpga.h"
#include <linux/spinlock.h>

static spinlock_t fpga_lock;
static unsigned long fpga_lock_flags;

void nbhw_fpga_lock_init(void)
{
	spin_lock_init(&fpga_lock);
}

void nbhw_fpga_lock(void)
{
	spin_lock_irqsave(&fpga_lock, fpga_lock_flags);
}

void nbhw_fpga_unlock(void)
{
	spin_unlock_irqrestore(&fpga_lock, fpga_lock_flags);
}
