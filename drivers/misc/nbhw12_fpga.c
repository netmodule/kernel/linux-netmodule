/*
 * nbhw12_fpga.c - Driver for FPGA on NBHW12 based Netbox designs
 *
 * Copyright (C) 2011-2015 NetModule AG
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/netbox.h>
#ifdef NBSW_TARGET_netbox_ppc
#include <linux/platform_device.h>
#include <linux/of_device.h>
#include <linux/proc_fs.h>
#include <linux/io.h>
#include <linux/ioport.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include "nbhw_fpga.h"

#define EXT_IRQ2_ID  18

extern void __init nbhw12_fpga_init(const char *compatible);

static void nbhw12_fpga_gpio_set(struct gpio_chip *gc, unsigned int gpio, int val);

static volatile void* fpga_base_virtual = 0;

static int fpga_virq = -1;
static int fpga_ready = 0;

struct nbhw12_gpio_data {
	struct gpio_chip gc;
	spinlock_t lock;

	/* shadowed data register to clear/set bits safely */
	u16 reg102; /* PCIe 1 slot control */
	u16 reg202; /* PCIe 2 slot control */
	/* Direction configuration */
	u16 dir102;
	u16 dir202;
};

static struct nbhw12_gpio_data *nbhw12_gd;

static int nbhw12_fpga_gpio_info(unsigned int gpio, u16** shadow_reg,
                                 u32* reg_addr, u16* reg_mask,
                                 u16** dir_mask)
{
	if ((gpio>=0) && (gpio<=5)) {
		*reg_mask = 0x0010; /* WAKE~*/
		switch (gpio) {
			case 0 :
				*reg_addr = FPGA_PCIE1_SLOT_CTRL;
				*dir_mask = &(nbhw12_gd->dir102);
				*shadow_reg = &(nbhw12_gd->reg102);
				break;
			case 1:
				*reg_addr = FPGA_PCIE2_SLOT_CTRL;
				*dir_mask = &(nbhw12_gd->dir202);
				*shadow_reg = &(nbhw12_gd->reg202);
				break;
			default:
				*reg_mask = 0;
				*reg_addr = 0;
				*dir_mask = 0;
				*shadow_reg = 0;
				return 0;
		}
	} else {
		*reg_mask = 0;
		*reg_addr = 0;
		*dir_mask = 0;
		*shadow_reg = 0;
		return 0;
	}
	return 1;
}

void nbhw12_fpga_set_reg(u32 reg, u16 value)
{
	if (!fpga_ready) return;
	*((volatile u16*)(fpga_base_virtual + reg)) = value;
}

u16 nbhw12_fpga_get_reg(u32 reg)
{
	u16 value = 0x0;
	value = *((volatile u16*)(fpga_base_virtual + reg));

	return value;
}

int nbhw12_fpga_get_irq(void)
{
	return fpga_virq;
}
EXPORT_SYMBOL(nbhw12_fpga_get_irq);


/* GPIO handling */

static int nbhw12_fpga_gpio_get(struct gpio_chip *gc, unsigned int gpio)
{
	u16* shadow_reg;
	u16 reg_mask;
	u16* dir_mask;
	u32 reg_addr;

	if (!nbhw12_fpga_gpio_info(gpio, &shadow_reg, &reg_addr, &reg_mask, &dir_mask)) return -1;

	if (*dir_mask & reg_mask)
	{
		/* is output */
		return ((*shadow_reg & reg_mask) != 0);
	} else {
		/* is input */
		return ((nbhw12_fpga_get_reg(reg_addr) & reg_mask) != 0);
	}
}

static void nbhw12_fpga_gpio_set(struct gpio_chip *gc, unsigned int gpio, int val)
{
	/* Not implemented. When implementing make sure that PCIe lines are not
	   driven when PCIe slot is powered down. -> Needs to be coordinated
	   with nbhw12_ext_gpio driver. */
	return;
}

static int nbhw12_fpga_gpio_dir_in(struct gpio_chip *gc, unsigned int gpio)
{
	/* Not implemented. GPIOs are always inputs now. */
	return 0;
}

static int nbhw12_fpga_gpio_dir_out(struct gpio_chip *gc, unsigned int gpio, int val)
{
	/* Not implemented. When implementing make sure that PCIe lines are not
	   driven when PCIe slot is powered down. -> Needs to be coordinated
	   with nbhw12_ext_gpio driver. */
	return 0;
}

void __init nbhw12_fpga_init(const char *compatible)
{
	int ret;
	struct gpio_chip *gc;

	u16 signature;
	char fpga_type[48];

	pr_debug("%s in\n", __FUNCTION__);

	if (request_mem_region(FPGA_BASE_ADDRESS, FPGA_BASE_SIZE ,"fpga") == NULL )
	{
		printk( KERN_ALERT "error:fpga: unable to obtain I/O memory address 0x%X\n",
			FPGA_BASE_ADDRESS );
		return;
	}

	fpga_base_virtual = (u8*)ioremap(FPGA_BASE_ADDRESS, FPGA_BASE_SIZE);
	if (!fpga_base_virtual) goto err;

	/* Check, if we have a valid FPGA */
	signature = nbhw12_fpga_get_reg(FPGA_IDENTIFICATION);
	if (signature == 0xa581)
	{
		strncpy(fpga_type, "XC3S50A-4VQ100I", sizeof(fpga_type));
		fpga_ready = 1;
	} else if (signature == 0xa582) {
		strncpy(fpga_type, "XC3S200A-4VQ100I", sizeof(fpga_type));
		fpga_ready = 1;
	} else {
		printk(KERN_ERR "Invalid FPGA signature %4.4x\n", nbhw12_fpga_get_reg(FPGA_IDENTIFICATION));
	}

	if (fpga_ready)
		printk("%s FPGA VERSION: %4.4x\n", fpga_type, nbhw12_fpga_get_reg(FPGA_VERSION));

	nbhw_fpga_lock_init();

	/* Register GPIOs */
	nbhw12_gd = kzalloc(sizeof(*nbhw12_gd), GFP_KERNEL);
	if (!nbhw12_gd) goto err;

	spin_lock_init(&nbhw12_gd->lock);

	/* Set defaults (All LEDs off, everything out of reset, enable PCIe clock) */
	nbhw12_gd->reg102 = 0x0020;
	nbhw12_gd->reg202 = 0x0020;

	nbhw12_gd->dir102 = 0x0020;
	nbhw12_gd->dir202 = 0x0020;

	/* init fpga register */
	nbhw12_fpga_set_reg(FPGA_PCIE1_SLOT_CTRL, nbhw12_gd->reg102);
	nbhw12_fpga_set_reg(FPGA_PCIE1_SLOT_DIR, nbhw12_gd->dir102);
	nbhw12_fpga_set_reg(FPGA_PCIE2_SLOT_CTRL, nbhw12_gd->reg202);
	nbhw12_fpga_set_reg(FPGA_PCIE2_SLOT_DIR, nbhw12_gd->dir202);

	gc = &nbhw12_gd->gc;
	gc->base = 64;
	gc->ngpio = 2;
	gc->direction_input = nbhw12_fpga_gpio_dir_in;
	gc->direction_output = nbhw12_fpga_gpio_dir_out;
	gc->get = nbhw12_fpga_gpio_get;
	gc->set = nbhw12_fpga_gpio_set;

	ret = gpiochip_add(gc);
	if (ret) goto err;

	fpga_virq = irq_create_mapping (NULL, EXT_IRQ2_ID);

	if (fpga_virq == NO_IRQ)
	{
		printk(KERN_ERR "nbhw12_fpga_init: could not map fpga irq\n");
		goto err;
	}
	irq_set_irq_type(fpga_virq, IRQ_TYPE_LEVEL_LOW);      // ! do it before request_irq ()

	pr_debug("%s out\n", __FUNCTION__);
	return;

err:
	pr_err("nbhw12_fpga_init: initialization failed!\n");
	if (fpga_base_virtual) iounmap(fpga_base_virtual);
	release_mem_region(FPGA_BASE_ADDRESS, FPGA_BASE_SIZE);
}

#endif /* NBSW_TARGET_netbox_ppc */
