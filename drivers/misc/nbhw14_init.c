/*
 * nbhw14_init.c - NBHW14 specific initializations
 *
 * Copyright (C) 2015 NetModule AG
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/kernel.h>
#include <linux/netbox.h>
#include <nbsw.h>

#if defined(NBSW_TARGET_netbolt_arm) || defined(NBSW_TARGET_netbird_arm)

int pcie_module_init(void);

int nbhw14_init_hw(void)
{
	pcie_module_init();

	return 0;
}

#endif
