/*
 * nbhw08_fpga.c - Driver for FPGA on NBHW08/14/17 based Netbox designs
 *
 * Copyright (C) 2011-2015 NetModule AG
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
#include <linux/platform_device.h>
#include <linux/of_device.h>
#include <linux/proc_fs.h>
#include <linux/io.h>
#include <linux/ioport.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/netbox.h>
#include "nbhw_fpga.h"
#include <linux/module.h>
#define NBHW 18

#ifdef NBSW_TARGET_netbox_ppc
#define EXT_IRQ0_ID  48
#elif defined(NBSW_TARGET_netbolt_arm)
#define EXT_IRQ_PIN 24
#else
#endif

#define FPGA_PGOOD (1<<15)

#define RS232_ENABLE (1<<0)
#define RS485_ENABLE (1<<1)
#define RS485_TERMINATION_ENABLE (1<<2)

extern void __init nbhw08_fpga_init(const char *compatible);

static void nbhw08_fpga_gpio_set(struct gpio_chip *gc, unsigned int gpio, int val);

static volatile void* fpga_base_virtual = 0;

static int fpga_virq = -1;
static int fpga_ready = 0;

struct nbhw08_gpio_data {
	struct gpio_chip gc;
	spinlock_t lock;

	/* shadowed data register to clear/set bits safely */
	u16 reg08; /* Mainboard Outputs */
	u16 reg20; /* LEDS */
	u16 reg30; /* PCIE reset */
	u16 reg32; /* PCIE power */
	u16 reg42; /* SMI ctrl */
	u16 reg50; /* PSE SSI Output */
	u16 reg60; /* PSE SSI Input */
	u16 reg102; /* PCIe 1 slot control */
	u16 reg202; /* PCIe 2 slot control */
	u16 reg302; /* PCIe 3 slot control */
	u16 reg402; /* PCIe 4 slot control */
	u16 reg502; /* PCIe 5 slot control */
	u16 reg602; /* PCIe 6 slot control */

	/* Direction configuration */
	u16 dir08;
	u16 dir20;
	u16 dir30;
	u16 dir32;
	u16 dir42;
	u16 dir50;
	u16 dir60;
	u16 dir102;
	u16 dir202;
	u16 dir302;
	u16 dir402;
	u16 dir502;
	u16 dir602;
};

static struct nbhw08_gpio_data *nbhw08_gd;

static struct timer_list pcie_power_restore_timer[6];

static int nbhw08_fpga_gpio_info(unsigned int gpio, u16** shadow_reg,
                                 u32* reg_addr, u16* reg_mask,
                                 u16** dir_mask)
{
	if ((gpio >= 0) && (gpio <= 15)) {
		*reg_mask = 1 << gpio;
		*reg_addr = FPGA_LED;
		*dir_mask = &(nbhw08_gd->dir20);
		*shadow_reg = &(nbhw08_gd->reg20);
	} else if ((gpio >= 16) && (gpio <= 23)) {
		*reg_mask = (1 << (gpio - 16));
		*reg_addr = FPGA_PCIE_RESET;
		*dir_mask = &(nbhw08_gd->dir30);
		*shadow_reg = &(nbhw08_gd->reg30);
	} else if ((gpio >= 24) && (gpio <= 31)) {
		*reg_mask = (1 << (gpio - 24));
		*reg_addr = FPGA_PCIE_POWER;
		*dir_mask = &(nbhw08_gd->dir32);
		*shadow_reg = &(nbhw08_gd->reg32);
	} else if ((gpio >= 32) && (gpio <= 39)) {
		*reg_mask = (1 << (gpio - 32));
		*reg_addr = FPGA_PSE_SSI_OUTPUT;
		*dir_mask = &(nbhw08_gd->dir50);
		*shadow_reg = &(nbhw08_gd->reg50);
	} else if ((gpio >= 40) && (gpio <= 47)) {
		*reg_mask = (1 << (gpio - 40));
		*reg_addr = FPGA_PSE_SSI_INPUT;
		*dir_mask = &(nbhw08_gd->dir60);
		*shadow_reg = &(nbhw08_gd->reg60);
	} else if (((gpio >= 48) && (gpio <= 53)) || ((gpio >= 65) && (gpio <= 70))) {
		if (gpio <= 53) {
			*reg_mask = 0x0020; /* WDIS~*/
			gpio -= 48;
		} else {
			*reg_mask = 0x0010; /* WAKE~*/
			gpio -= 65;
		}
		switch (gpio) {
			case 0 :
				*reg_addr = FPGA_PCIE1_SLOT_CTRL;
				*dir_mask = &(nbhw08_gd->dir102);
				*shadow_reg = &(nbhw08_gd->reg102);
				break;
			case 1:
				*reg_addr = FPGA_PCIE2_SLOT_CTRL;
				*dir_mask = &(nbhw08_gd->dir202);
				*shadow_reg = &(nbhw08_gd->reg202);
				break;
			case 2:
				*reg_addr = FPGA_PCIE3_SLOT_CTRL;
				*dir_mask = &(nbhw08_gd->dir302);
				*shadow_reg = &(nbhw08_gd->reg302);
				break;
			case 3:
				*reg_addr = FPGA_PCIE4_SLOT_CTRL;
				*dir_mask = &(nbhw08_gd->dir402);
				*shadow_reg = &(nbhw08_gd->reg402);
				break;
			case 4:
				*reg_addr = FPGA_PCIE5_SLOT_CTRL;
				*dir_mask = &(nbhw08_gd->dir502);
				*shadow_reg = &(nbhw08_gd->reg502);
				break;
			case 5:
				*reg_addr = FPGA_PCIE6_SLOT_CTRL;
				*dir_mask = &(nbhw08_gd->dir602);
				*shadow_reg = &(nbhw08_gd->reg602);
				break;
			default:
				*reg_mask = 0;
				*reg_addr = 0;
				*dir_mask = 0;
				*shadow_reg = 0;
				return 0;
		}
	} else if (((NBHW == 17) || (NBHW == 18)) && (gpio >= 56) && (gpio <= 64)) {
		/* Only available on NBHW17 */
		*reg_mask = (1 << (gpio - 56));
		*reg_addr = FPGA_OUTPUT1;
		*dir_mask = &(nbhw08_gd->dir08);
		*shadow_reg = &(nbhw08_gd->reg08);
	} else if ((NBHW == 18) && (gpio >= 72) && (gpio <= 80)) {
		/* Only available on NBHW18 */
		*reg_mask = (1 << (gpio - 72));
		*reg_addr = FPGA_SMI_CTRL;
		*dir_mask = &(nbhw08_gd->dir42);
		*shadow_reg = &(nbhw08_gd->reg42);
	}
	else {
		*reg_mask = 0;
		*reg_addr = 0;
		*dir_mask = 0;
		*shadow_reg = 0;
		return 0;
	}
	return 1;
}

void nbhw08_fpga_set_reg(u32 reg, u16 value)
{
	if (!fpga_ready) return;
	*((volatile u16*)(fpga_base_virtual + reg)) = value;
}
EXPORT_SYMBOL(nbhw08_fpga_set_reg);

u16 nbhw08_fpga_get_reg(u32 reg)
{
	u16 value = 0x0;
	value = *((volatile u16*)(fpga_base_virtual + reg));

	return value;
}
EXPORT_SYMBOL(nbhw08_fpga_get_reg);

int nbhw08_fpga_get_irq(void)
{
	return fpga_virq;
}
EXPORT_SYMBOL(nbhw08_fpga_get_irq);

static void prepare_pci_slot_for_power_down(struct gpio_chip *gc, int slot)
{
	int reset_low_value = 0;

	if (NBHW==18) {
		/* Because on NBHW18 reset polarity is inverted we need this ugly hack */
		reset_low_value = 1;
	}

	switch (slot) {
		case 0 :
			nbhw08_fpga_gpio_set(gc, 16, reset_low_value);
			nbhw08_fpga_set_reg(FPGA_PCIE1_SLOT_DIR, 0x0000);
			break;
		case 1 :
			nbhw08_fpga_gpio_set(gc, 17, reset_low_value);
			nbhw08_fpga_set_reg(FPGA_PCIE2_SLOT_DIR, 0x0000);
			break;
		case 2 :
			nbhw08_fpga_gpio_set(gc, 18, reset_low_value);
			nbhw08_fpga_set_reg(FPGA_PCIE3_SLOT_DIR, 0x0000);
			break;
		case 3 :
			nbhw08_fpga_gpio_set(gc, 19, reset_low_value);
			nbhw08_fpga_set_reg(FPGA_PCIE4_SLOT_DIR, 0x0000);
			break;
		case 4 :
			nbhw08_fpga_gpio_set(gc, 20, reset_low_value);
			nbhw08_fpga_set_reg(FPGA_PCIE5_SLOT_DIR, 0x0000);
			break;
		case 5 :
			nbhw08_fpga_gpio_set(gc, 21, reset_low_value);
			nbhw08_fpga_set_reg(FPGA_PCIE6_SLOT_DIR, 0x0000);
			break;
	}
}

static void restore_pci_slot_after_power_up(unsigned long slot)
{
	int reset_high_value = 1;

	if (NBHW==18) {
		/* Because on NBHW18 reset polarity is inverted we need this ugly hack */
		reset_high_value = 0;
	}

	switch (slot) {
		case 0 :
			nbhw08_fpga_set_reg(FPGA_PCIE1_SLOT_DIR, nbhw08_gd->dir102);
			nbhw08_fpga_set_reg(FPGA_PCIE1_SLOT_CTRL, nbhw08_gd->reg102);
			nbhw08_fpga_gpio_set(&(nbhw08_gd->gc), 16, reset_high_value);
			break;
		case 1 :
			nbhw08_fpga_set_reg(FPGA_PCIE2_SLOT_DIR, nbhw08_gd->dir202);
			nbhw08_fpga_set_reg(FPGA_PCIE2_SLOT_CTRL, nbhw08_gd->reg202);
			nbhw08_fpga_gpio_set(&(nbhw08_gd->gc), 17, reset_high_value);
			break;
		case 2 :
			nbhw08_fpga_set_reg(FPGA_PCIE3_SLOT_DIR, nbhw08_gd->dir302);
			nbhw08_fpga_set_reg(FPGA_PCIE3_SLOT_CTRL, nbhw08_gd->reg302);
			nbhw08_fpga_gpio_set(&(nbhw08_gd->gc), 18, reset_high_value);
			break;
		case 3 :
			nbhw08_fpga_set_reg(FPGA_PCIE4_SLOT_DIR, nbhw08_gd->dir402);
			nbhw08_fpga_set_reg(FPGA_PCIE4_SLOT_CTRL, nbhw08_gd->reg402);
			nbhw08_fpga_gpio_set(&(nbhw08_gd->gc), 19, reset_high_value);
			break;
		case 4 :
			nbhw08_fpga_set_reg(FPGA_PCIE5_SLOT_DIR, nbhw08_gd->dir502);
			nbhw08_fpga_set_reg(FPGA_PCIE5_SLOT_CTRL, nbhw08_gd->reg502);
			nbhw08_fpga_gpio_set(&(nbhw08_gd->gc), 20, reset_high_value);
			break;
		case 5 :
			nbhw08_fpga_set_reg(FPGA_PCIE6_SLOT_DIR, nbhw08_gd->dir602);
			nbhw08_fpga_set_reg(FPGA_PCIE6_SLOT_CTRL, nbhw08_gd->reg602);
			nbhw08_fpga_gpio_set(&(nbhw08_gd->gc), 21, reset_high_value);
			break;
	}
}

/* GPIO handling */

static int nbhw08_fpga_gpio_get(struct gpio_chip *gc, unsigned int gpio)
{
	u16* shadow_reg;
	u16 reg_mask;
	u16* dir_mask;
	u32 reg_addr;

	if (!nbhw08_fpga_gpio_info(gpio, &shadow_reg, &reg_addr, &reg_mask, &dir_mask)) {
		return -1;
	}
	if (*dir_mask & reg_mask) {
		/* is output */
		return ((*shadow_reg & reg_mask) != 0);
	} else {
		/* is input */
		return ((nbhw08_fpga_get_reg(reg_addr) & reg_mask) != 0);
	}
}

static void nbhw08_fpga_gpio_set(struct gpio_chip *gc, unsigned int gpio, int val)
{
	u16* shadow_reg;
	u16 reg_mask;
	u16* dir_mask;
	u32 reg_addr;
	unsigned long flags;
	int power_control_for_slot = -1;

	if (!nbhw08_fpga_gpio_info(gpio, &shadow_reg, &reg_addr, &reg_mask, &dir_mask)) {
		return;
	}

	if ((*dir_mask & reg_mask) == 0) {
		/* is input */
		return;
	}
	if (reg_addr == FPGA_PCIE_POWER) {
		switch (reg_mask) {
			case 0x0001 :
				power_control_for_slot = 0;
				break;
			case 0x0002 :
				power_control_for_slot = 1;
				break;
			case 0x0004 :
				power_control_for_slot = 2;
				break;
			case 0x0008 :
				power_control_for_slot = 3;
				break;
			case 0x0010 :
				power_control_for_slot = 4;
				break;
			case 0x0020 :
				power_control_for_slot = 5;
				break;
		}
		if (val == 0) {
			prepare_pci_slot_for_power_down(gc, power_control_for_slot);
			msleep(10); /* Actually not needed, just to get nice graphs on scope */
		}
	}

	spin_lock_irqsave(&nbhw08_gd->lock, flags);

	if (val) {
		*shadow_reg |= reg_mask;
	} else {
		*shadow_reg &= ~reg_mask;
	}

	nbhw08_fpga_set_reg(reg_addr, *shadow_reg);

	if ((power_control_for_slot >= 0) && (val == 1)) {
		init_timer(&pcie_power_restore_timer[power_control_for_slot]);
		pcie_power_restore_timer[power_control_for_slot].function = restore_pci_slot_after_power_up;
		pcie_power_restore_timer[power_control_for_slot].expires = jiffies + (HZ / 50); // after 20ms
		pcie_power_restore_timer[power_control_for_slot].data = (unsigned long)power_control_for_slot;
		add_timer(&pcie_power_restore_timer[power_control_for_slot]);
	}

	spin_unlock_irqrestore(&nbhw08_gd->lock, flags);
}

static int nbhw08_fpga_gpio_dir_in(struct gpio_chip *gc, unsigned int gpio)
{
	u16 reg_mask;

	if (((gpio >= 48) && (gpio <= 53)) || ((gpio >= 65) && (gpio <= 70))) {
		if (gpio <= 53) {
			reg_mask = 0x0020; /* WDIS~*/
			gpio -= 48;
		} else {
			reg_mask = 0x0010; /* WAKE~*/
			gpio -= 65;
		}
		switch (gpio) {
			case 0 :
				nbhw08_gd->dir102 &= reg_mask;
				nbhw08_fpga_set_reg(FPGA_PCIE1_SLOT_DIR, nbhw08_gd->dir102);
				break;
			case 1:
				nbhw08_gd->dir202 &= reg_mask;
				nbhw08_fpga_set_reg(FPGA_PCIE2_SLOT_DIR, nbhw08_gd->dir202);
				break;
			case 2:
				nbhw08_gd->dir302 &= reg_mask;
				nbhw08_fpga_set_reg(FPGA_PCIE3_SLOT_DIR, nbhw08_gd->dir302);
				break;
			case 3:
				nbhw08_gd->dir402 &= reg_mask;
				nbhw08_fpga_set_reg(FPGA_PCIE4_SLOT_DIR, nbhw08_gd->dir402);
				break;
			case 4:
				nbhw08_gd->dir502 &= reg_mask;
				nbhw08_fpga_set_reg(FPGA_PCIE5_SLOT_DIR, nbhw08_gd->dir502);
				break;
			case 5:
				nbhw08_gd->dir602 &= reg_mask;
				nbhw08_fpga_set_reg(FPGA_PCIE6_SLOT_DIR, nbhw08_gd->dir602);
				break;
			default:
				break;
		}
	}

	return 0;
}

static int nbhw08_fpga_gpio_dir_out(struct gpio_chip *gc, unsigned int gpio, int val)
{
	u16 reg_mask;

	if (((gpio >= 48) && (gpio <= 53)) || ((gpio >= 65) && (gpio <= 70))) {
		if (gpio <= 53) {
			reg_mask = 0x0020; /* WDIS~*/
			gpio -= 48;
		} else {
			reg_mask = 0x0010; /* WAKE~*/
			gpio -= 65;
		}
		switch (gpio) {
			case 0 :
				nbhw08_gd->dir102 |= reg_mask;
				nbhw08_fpga_set_reg(FPGA_PCIE1_SLOT_CTRL, nbhw08_gd->reg102);
				nbhw08_fpga_set_reg(FPGA_PCIE1_SLOT_DIR, nbhw08_gd->dir102);
				break;
			case 1:
				nbhw08_gd->dir202 |= reg_mask;
				nbhw08_fpga_set_reg(FPGA_PCIE2_SLOT_CTRL, nbhw08_gd->reg202);
				nbhw08_fpga_set_reg(FPGA_PCIE2_SLOT_DIR, nbhw08_gd->dir202);
				break;
			case 2:
				nbhw08_gd->dir302 |= reg_mask;
				nbhw08_fpga_set_reg(FPGA_PCIE3_SLOT_CTRL, nbhw08_gd->reg302);
				nbhw08_fpga_set_reg(FPGA_PCIE3_SLOT_DIR, nbhw08_gd->dir302);
				break;
			case 3:
				nbhw08_gd->dir402 |= reg_mask;
				nbhw08_fpga_set_reg(FPGA_PCIE4_SLOT_CTRL, nbhw08_gd->reg402);
				nbhw08_fpga_set_reg(FPGA_PCIE4_SLOT_DIR, nbhw08_gd->dir402);
				break;
			case 4:
				nbhw08_gd->dir502 |= reg_mask;
				nbhw08_fpga_set_reg(FPGA_PCIE5_SLOT_CTRL, nbhw08_gd->reg502);
				nbhw08_fpga_set_reg(FPGA_PCIE5_SLOT_DIR, nbhw08_gd->dir502);
				break;
			case 5:
				nbhw08_gd->dir602 |= reg_mask;
				nbhw08_fpga_set_reg(FPGA_PCIE6_SLOT_CTRL, nbhw08_gd->reg602);
				nbhw08_fpga_set_reg(FPGA_PCIE6_SLOT_DIR, nbhw08_gd->dir602);
				break;
			default:
				break;
		}
	}

	return 0;
}

void __init nbhw08_fpga_init(const char *compatible)
{
#ifdef NBSW_TARGET_netbox_ppc
	int ret;
	struct gpio_chip *gc;
#endif
	u16 signature;
	char fpga_type[48];

	printk(KERN_DEBUG "Initializing FPGA\n");

	if (request_mem_region(FPGA_BASE_ADDRESS, FPGA_BASE_SIZE ,"fpga") == NULL) {
		printk(KERN_ALERT "unable to obtain I/O memory address 0x%X for FPGA\n",
				  FPGA_BASE_ADDRESS);
		return;
	}

	fpga_base_virtual = (u8*)ioremap(FPGA_BASE_ADDRESS, FPGA_BASE_SIZE);
	if (!fpga_base_virtual) {
		printk(KERN_ALERT "unable to remap I/O memory address 0x%X for FPGA\n",
				  FPGA_BASE_ADDRESS);
		goto err;
	}

	/* Check, if we have a valid FPGA */
	signature = nbhw08_fpga_get_reg(FPGA_IDENTIFICATION);
	if (signature == 0xa501) {
		strncpy(fpga_type, "XC3S50A-4VQ100I top layer", sizeof(fpga_type));
		fpga_ready = 1;
	} else if (signature == 0xa502) {
		strncpy(fpga_type, "XC3S200A-4VQ100I top layer", sizeof(fpga_type));
		fpga_ready = 1;
	} else if (signature == 0xa511) {
		strncpy(fpga_type, "XC3S50A-4FTG256I bottom layer", sizeof(fpga_type));
		fpga_ready = 1;
	} else if (signature == 0xa512) {
		strncpy(fpga_type, "XC3S200A-4FTG256I bottom layer", sizeof(fpga_type));
		fpga_ready = 1;
	} else if (signature == 0x4004) {
		strncpy(fpga_type, "ICE40HX4K-CB132 NBHW17", sizeof(fpga_type));
		fpga_ready = 1;
	} else if (signature == 0x012f) {
		strncpy(fpga_type, "LFE5U-12F-6BG381I NBHW18 V2", sizeof(fpga_type));
		fpga_ready = 1;
	} else {
		printk(KERN_ERR "Invalid FPGA signature %4.4x\n", nbhw08_fpga_get_reg(FPGA_IDENTIFICATION));
	}

	if (fpga_ready) {
		printk("%s FPGA VERSION: %4.4x\n", fpga_type, nbhw08_fpga_get_reg(FPGA_VERSION));
	}

	nbhw_fpga_lock_init();

	/* Register GPIOs */
	nbhw08_gd = kzalloc(sizeof(*nbhw08_gd), GFP_KERNEL);
	if (!nbhw08_gd) goto err;

	spin_lock_init(&nbhw08_gd->lock);


	/* Set defaults (All LEDs off, everything out of reset, enable PCIe clock) */
	nbhw08_gd->reg08 = nbhw08_fpga_get_reg(FPGA_OUTPUT1);
	nbhw08_gd->reg20 = 0x0001;
	nbhw08_gd->reg30 = nbhw08_fpga_get_reg(FPGA_PCIE_RESET); /* Keep initialization done by U-Boot. (As polarity depends on HW) */
	nbhw08_gd->reg32 = nbhw08_fpga_get_reg(FPGA_PCIE_POWER); /* Keep initialization done by U-Boot. Especially PCIE1 clock setting */
	nbhw08_gd->reg42 = nbhw08_fpga_get_reg(FPGA_SMI_CTRL); /* Keep initialization done by U-Boot. Especially PCIE1 clock setting */
	nbhw08_gd->reg50 = 0x0008; /* Enable SATA power, disable DIOs */
	nbhw08_gd->reg60 = 0x0000;
	nbhw08_gd->reg102 = 0x0020;
	nbhw08_gd->reg202 = 0x0020;
	nbhw08_gd->reg302 = 0x0020;
	nbhw08_gd->reg402 = 0x0020;
	nbhw08_gd->reg502 = 0x0020;
	nbhw08_gd->reg602 = 0x0020;

	nbhw08_gd->dir08 = 0xffff;
#ifdef NBSW_TARGET_netbox_ppc
	nbhw08_gd->dir20 = 0x3fff;  /* 7 LEDs */
#elif defined(NBSW_TARGET_netbolt_arm)
	nbhw08_gd->dir20 = 0xffff;  /* 8 LEDs */
#endif
	nbhw08_gd->dir30 = 0x003f;
	nbhw08_gd->dir32 = 0x00bf;
	nbhw08_gd->dir42 = 0x0003;
	nbhw08_gd->dir50 = 0xffff;
	nbhw08_gd->dir60 = 0x0000;
	nbhw08_gd->dir102 = 0x0020;
	nbhw08_gd->dir202 = 0x0020;
	nbhw08_gd->dir302 = 0x0020;
	nbhw08_gd->dir402 = 0x0020;
	nbhw08_gd->dir502 = 0x0020;
	nbhw08_gd->dir602 = 0x0020;

	/* init fpga register */
	/* Do not touch reg08 because it is set by u-boot correctly, there the decision is made
	 * how the mux is set up */
	nbhw08_fpga_set_reg(FPGA_LED, nbhw08_gd->reg20);
	nbhw08_fpga_set_reg(FPGA_PCIE_RESET, nbhw08_gd->reg30);
	nbhw08_fpga_set_reg(FPGA_PSE_SSI_OUTPUT, nbhw08_gd->reg50);
	nbhw08_fpga_set_reg(FPGA_PSE_SSI_INPUT, nbhw08_gd->reg60);
	nbhw08_fpga_set_reg(FPGA_PCIE1_SLOT_CTRL, nbhw08_gd->reg102);
	nbhw08_fpga_set_reg(FPGA_PCIE1_SLOT_DIR, nbhw08_gd->dir102);
	nbhw08_fpga_set_reg(FPGA_PCIE2_SLOT_CTRL, nbhw08_gd->reg202);
	nbhw08_fpga_set_reg(FPGA_PCIE2_SLOT_DIR, nbhw08_gd->dir202);
	nbhw08_fpga_set_reg(FPGA_PCIE3_SLOT_CTRL, nbhw08_gd->reg302);
	nbhw08_fpga_set_reg(FPGA_PCIE3_SLOT_DIR, nbhw08_gd->dir302);
	nbhw08_fpga_set_reg(FPGA_PCIE4_SLOT_CTRL, nbhw08_gd->reg402);
	nbhw08_fpga_set_reg(FPGA_PCIE4_SLOT_DIR, nbhw08_gd->dir402);
	nbhw08_fpga_set_reg(FPGA_PCIE5_SLOT_CTRL, nbhw08_gd->reg502);
	nbhw08_fpga_set_reg(FPGA_PCIE5_SLOT_DIR, nbhw08_gd->dir502);
	nbhw08_fpga_set_reg(FPGA_PCIE6_SLOT_CTRL, nbhw08_gd->reg602);
	nbhw08_fpga_set_reg(FPGA_PCIE6_SLOT_DIR, nbhw08_gd->dir602);

#ifdef NBSW_TARGET_netbox_ppc
	gc = &nbhw08_gd->gc;
	gc->base = 0;
	gc->ngpio = 71;
	gc->direction_input = nbhw08_fpga_gpio_dir_in;
	gc->direction_output = nbhw08_fpga_gpio_dir_out;
	gc->get = nbhw08_fpga_gpio_get;
	gc->set = nbhw08_fpga_gpio_set;

	ret = gpiochip_add(gc);
	if (ret) goto err;
#endif

#ifdef NBSW_TARGET_netbox_ppc
	fpga_virq = irq_create_mapping (NULL, EXT_IRQ0_ID);
#elif defined(NBSW_TARGET_netbolt_arm)
	fpga_virq = gpio_to_irq(EXT_IRQ_PIN);
#else
#endif

	if (fpga_virq == NO_IRQ) {
		printk(KERN_ERR "could not map FPGA irq\n");
		goto err;
	}
	irq_set_irq_type(fpga_virq, IRQ_TYPE_LEVEL_LOW);      // ! do it before request_irq ()


	/* Clear power good status interrupt flag, it is set to 1 after boot */
	iowrite16(FPGA_PGOOD, fpga_base_virtual + FPGA_INT_ACK_1);

	return;

err:
	pr_err("unable to initialize FPGA\n");
	if (fpga_base_virtual) iounmap(fpga_base_virtual);
	release_mem_region(FPGA_BASE_ADDRESS, FPGA_BASE_SIZE);
}

#ifdef NBSW_TARGET_netbolt_arm

struct nbhw08_fpga_gpio_chip {
	struct gpio_chip   chip;
};

static const struct of_device_id nbhw08_fpga_gpio_of_match[] = {
	{
		.compatible = "netmodule,nbhw08-fpga-gpio",
		.data       = (void *)0,
	},
};
MODULE_DEVICE_TABLE(of, nbhw08_fpga_gpio_of_match);

static int nbhw08_fpga_gpio_probe(struct platform_device *pdev)
{
	struct nbhw08_fpga_gpio_chip *nbchip;
	const struct of_device_id *match;
	struct device_node *np = pdev->dev.of_node;
	unsigned int ngpios;

	match = of_match_device(nbhw08_fpga_gpio_of_match, &pdev->dev);
	if (!match) return 0;

	nbhw08_fpga_init("nbhw14-fpga");

	nbchip = devm_kzalloc(&pdev->dev, sizeof(struct nbhw08_fpga_gpio_chip), GFP_KERNEL);
	if (!nbchip)
		return -ENOMEM;

	platform_set_drvdata(pdev, nbchip);

	if (of_property_read_u32(pdev->dev.of_node, "ngpios", &ngpios)) {
		dev_err(&pdev->dev, "Missing ngpios OF property\n");
		return -ENODEV;
	}

	nbchip->chip.label = dev_name(&pdev->dev);
	nbchip->chip.parent = &pdev->dev;
	nbchip->chip.direction_input = nbhw08_fpga_gpio_dir_in;
	nbchip->chip.get = nbhw08_fpga_gpio_get;
	nbchip->chip.direction_output = nbhw08_fpga_gpio_dir_out;
	nbchip->chip.set = nbhw08_fpga_gpio_set;
	nbchip->chip.base = 64;
	nbchip->chip.ngpio = ngpios;
	nbchip->chip.can_sleep = false;
	nbchip->chip.of_node = np;

	gpiochip_add(&nbchip->chip);

	return 0;
}

static ssize_t scratch_store(struct device_driver *driver,const char *buf, size_t count)
{
	unsigned long val;
	int ret;

	ret = kstrtoul(buf, 0, &val);
	if (ret) {
		printk(KERN_ERR "Can not convert string %s\n to int", buf);
		return -ret;
	}

	iowrite16((u16)val, fpga_base_virtual + FPGA_SCRATCH);
	return count;
}

static ssize_t scratch_show(struct device_driver *driver, char *buf)
{
	u16 val;

	val = ioread16(fpga_base_virtual + FPGA_SCRATCH);

	return snprintf(buf, PAGE_SIZE, "0x%04X", val);
}

DRIVER_ATTR_RW(scratch);

static ssize_t pgood_store(struct device_driver *driver,const char *buf, size_t count)
{
	unsigned long val;
	int ret;

	ret = kstrtoul(buf, 0, &val);
	if (ret) {
		printk(KERN_ERR "Can not convert string %s\n to int", buf);
		return -ret;
	}

	/* Clear power good status interrupt => clear error */
	if (val == 1) {
		iowrite16(FPGA_PGOOD, fpga_base_virtual + FPGA_INT_ACK_1);
	}

	return count;
}

static ssize_t pgood_show(struct device_driver *driver, char *buf)
{
	u16 val;

	/* Read the current PGOOD interrupt status. One toggle means we had an
	 * interrupt which means we had a power fail */
	val = ioread16(fpga_base_virtual + FPGA_INT_STATUS_1) & FPGA_PGOOD ? 0 : 1;

	/* NBHW18 does not have a power good detection... */
	if (NBHW == 18)
		val = 1;

	return snprintf(buf, PAGE_SIZE, "%hu", val);
}

DRIVER_ATTR_RW(pgood);

static ssize_t rs485_enable_store(struct device_driver *driver,const char *buf, size_t count)
{
	unsigned long val;
	int ret;
	u16 reg;

	if (NBHW != 18) return -1;

	ret = kstrtoul(buf, 0, &val);
	if (ret) {
		printk(KERN_ERR "Can not convert string %s\n to int", buf);
		return -ret;
	}

	reg = ioread16(fpga_base_virtual + FPGA_SERIAL_CTRL);
	reg &= ~((u16)(RS232_ENABLE | RS485_ENABLE));
	if (val == 1) {
		reg |= RS485_ENABLE;
	} else {
		reg |= RS232_ENABLE;
	}
	iowrite16(reg, fpga_base_virtual + FPGA_SERIAL_CTRL);

	return count;
}

static ssize_t rs485_enable_show(struct device_driver *driver, char *buf)
{
	int val;

	if (NBHW == 18) {
		val = (ioread16(fpga_base_virtual + FPGA_SERIAL_CTRL) & RS485_ENABLE) ? 1 : 0;
	} else {
		val = -1;
	}

	return snprintf(buf, PAGE_SIZE, "%d", val);
}

DRIVER_ATTR_RW(rs485_enable);

static ssize_t rs485_termination_enable_store(struct device_driver *driver,const char *buf, size_t count)
{
	unsigned long val;
	int ret;
	u16 reg;

	if (NBHW != 18) return -1;

	ret = kstrtoul(buf, 0, &val);
	if (ret) {
		printk(KERN_ERR "Can not convert string %s\n to int", buf);
		return -ret;
	}

	reg = ioread16(fpga_base_virtual + FPGA_SERIAL_CTRL);
	reg &= ~((u16)(RS485_TERMINATION_ENABLE));
	if (val == 1) {
		reg |= RS485_TERMINATION_ENABLE;
	}
	iowrite16(reg, fpga_base_virtual + FPGA_SERIAL_CTRL);

	return count;
}

static ssize_t rs485_termination_enable_show(struct device_driver *driver, char *buf)
{
	int val;

	if (NBHW == 18) {
		val = (ioread16(fpga_base_virtual + FPGA_SERIAL_CTRL) & RS485_TERMINATION_ENABLE) ? 1 : 0;
	} else {
		val = -1;
	}

	return snprintf(buf, PAGE_SIZE, "%d", val);
}

DRIVER_ATTR_RW(rs485_termination_enable);

static struct attribute *fpga_info_attrs[] = {
	&driver_attr_scratch.attr,
	&driver_attr_pgood.attr,
	&driver_attr_rs485_enable.attr,
	&driver_attr_rs485_termination_enable.attr,
	NULL
};

ATTRIBUTE_GROUPS(fpga_info);

static struct platform_driver nbhw08_fpga_gpio_driver = {
	.driver		= {
		.name           = "nbhw08-fpga-gpio",
		.owner          = THIS_MODULE,
		.of_match_table = nbhw08_fpga_gpio_of_match,
		.groups = fpga_info_groups
	},
	.probe		= nbhw08_fpga_gpio_probe,
};
module_platform_driver(nbhw08_fpga_gpio_driver);

#endif
